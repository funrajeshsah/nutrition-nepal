package com.nepget.rhino;

import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

/**
 * Created by umesh on 25-07-2016.
 */
public class BaseActivity  extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener{
    private String TAG = "BaseActivity";
    public DrawerLayout drawer;
    public ActionBarDrawerToggle toggle;
    public NavigationView navigationView;
    public Toolbar toolbar;
    protected void onCreateDrawer() {
       // super.onCreate(savedInstanceState);
        LocaleHelper.onCreate(this);
        //setContentView(R.layout.activity_home);
         toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

         drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
         toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

         navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }


    public void onClickAge(View view){
        String age_group = "";
        Intent intent = null;
        Log.v("sendMsg", "before intent");
        if ((view.getId() == R.id.radioButtonLessThen2Month)){
            intent = new Intent(this, LessThen2MonthActivity.class);
        }else {
            intent = new Intent(this, MoreThen2MonthActivity.class);
        }

        intent.putExtra("question_metrics", age_group);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.english_language) {
            LocaleHelper.setLocale(this, "en");
            updateViews();
            return true;
        }
        if (id == R.id.action_logout) {
            FirebaseAuth mFirebaseAuth;
            FirebaseUser mFirebaseUser;

            mFirebaseAuth = FirebaseAuth.getInstance();
            mFirebaseUser = mFirebaseAuth.getCurrentUser();

            mFirebaseAuth.signOut();
            loadLogInView();
        }
        /*else if (id == R.id.english_language) {
           Log.d(TAG, "inside settings english");
           LocaleHelper.setLocale(this, "en");
           updateViews();
           return true;
        }*/
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        /*if (id == R.id.nav_principle_of_imnci) {
            Log.v("sendMsg", "before intent");
            Intent intent = new Intent(this, NavResult.class);
            intent.putExtra("item_name", "nav_principle_of_imnci");
            startActivity(intent);
        }else */
        if (id == R.id.nav_home) {
            Log.v("sendMsg", "before intent");
            Intent intent = new Intent(this, Home.class);
            //Added this to exit from home when back is pressed (clear backstack)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish(); // call this to finish the current activity
        }
        else if (id == R.id.about_us) {
            Log.v("sendMsg", "before intent");
            Intent intent = new Intent(this, NavResult.class);
            intent.putExtra("item_name", "about_us");
            startActivity(intent);
        }
        else if (id == R.id.contact_us) {
            Log.v("sendMsg", "before intent");
            Intent intent = new Intent(this, NavResult.class);
            intent.putExtra("item_name", "contact_us");
            startActivity(intent);
        }
        else if (id == R.id.terms_and_condition) {
            Log.v("sendMsg", "before intent");
            Intent intent = new Intent(this, NavResult.class);
            intent.putExtra("item_name", "terms_and_condition");
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void updateViews(){
        recreate();
    }

    private void loadLogInView() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
}
