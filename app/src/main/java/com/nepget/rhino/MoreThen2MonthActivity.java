package com.nepget.rhino;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.ScaleGestureDetector;
import android.graphics.Matrix;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;

import com.github.jjobes.htmldialog.HtmlDialog;
import com.nepget.rhino.utility.Data;

public class MoreThen2MonthActivity extends BaseActivity {

    private String TAG = "MoreThen2MonthActivity";
    public static Data question_metrics;
    ScaleGestureDetector scaleGestureDetector;
    Matrix matrix = new Matrix();
    TouchImageView image;
    SwitchCompat switch_danger,edima_switch,wt_switch,appetite_switch,med_complication_switch;
    WebView webView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LocaleHelper.onCreate(this);
        setContentView(R.layout.activity_home);
        super.onCreateDrawer();
        FrameLayout frame = (FrameLayout) findViewById(R.id.containt_framme);
        View childView =  getLayoutInflater().inflate(R.layout.content_main,null);
        frame.addView(childView);
        question_metrics = new Data();

        switch_danger = (SwitchCompat) findViewById(R.id.danger_switch);
        switch_danger.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
                View viewcard;

                if(isChecked){
                    viewcard=(View)findViewById(R.id.view_danger);
                    viewcard.setVisibility(View.VISIBLE);

                }else{
                    viewcard=(View)findViewById(R.id.view_danger);
                    viewcard.setVisibility(View.GONE);
                }

            }
        });

        edima_switch = (SwitchCompat) findViewById(R.id.edima_switch);
        edima_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
                View viewcard;

                if(isChecked){
                    viewcard=(View)findViewById(R.id.view_edima);
                    viewcard.setVisibility(View.VISIBLE);

                }else{
                    viewcard=(View)findViewById(R.id.view_edima);
                    viewcard.setVisibility(View.GONE);
                }

            }
        });

        wt_switch = (SwitchCompat) findViewById(R.id.wt_switch);
        wt_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
                View viewcard;

                if(isChecked){
                    viewcard=(View)findViewById(R.id.view_wt);
                    viewcard.setVisibility(View.VISIBLE);

                }else{
                    viewcard=(View)findViewById(R.id.view_wt);
                    viewcard.setVisibility(View.GONE);
                }

            }
        });

        appetite_switch = (SwitchCompat) findViewById(R.id.appetite_switch);
        appetite_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
                View viewcard;

                if(isChecked){
                    viewcard=(View)findViewById(R.id.view_appetite);
                    viewcard.setVisibility(View.VISIBLE);

                }else{
                    viewcard=(View)findViewById(R.id.view_appetite);
                    viewcard.setVisibility(View.GONE);
                }

            }
        });

        med_complication_switch = (SwitchCompat) findViewById(R.id.med_complication_switch);
        med_complication_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
                View viewcard;

                if(isChecked){
                    viewcard=(View)findViewById(R.id.view_med_complication);
                    viewcard.setVisibility(View.VISIBLE);

                }else{
                    viewcard=(View)findViewById(R.id.view_med_complication);
                    viewcard.setVisibility(View.GONE);
                }

            }
        });


    }




    public void sendMessage(View view){

        Log.v("sendMsg", "before intent");
        Intent intent = new Intent(this, DisplayMessageActivity.class);
        Log.v("sendMsg", "before intent");
        intent.putExtra("lessThen2", "false");
        intent.putExtra("question_metrics", question_metrics);
        Log.v("sendMsg", "after intent");
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.nepali_language) {
            LocaleHelper.setLocale(this, "en");
            updateViews();
            return true;
        }
        /*else if (id == R.id.english_language) {
            Log.d(TAG, "inside settings english");
            LocaleHelper.setLocale(this, "en");
            updateViews();
            return true;
        }*/
        return super.onOptionsItemSelected(item);
    }


    public void helpDetail(View view){
        String filePath = "";
        switch(view.getId()) {
            case R.id.help_med_complication:
                filePath = "file:///android_res/raw/help_med_complication.html";
                break;
            case R.id.help_edema:
                filePath = "file:///android_res/raw/help_edema.html";
                break;
            case R.id.help_wt_for_height:
                filePath = "file:///android_res/raw/help_wt_for_height.html";
                break;
        }

        if (!filePath.equalsIgnoreCase("")){
            webView = new WebView(this);
            webView.loadUrl(filePath);
            webView.getSettings().setUseWideViewPort(true);
            AlertDialog.Builder alert1 = new AlertDialog.Builder(this);
            alert1.setView(webView);
            alert1.show();
        }

    }


    public void onCheckboxClicked(View view) {



        // Is the view now checked?
        boolean checked = ((CheckBox) view).isChecked();

        // Check which checkbox was clicked
        switch(view.getId()) {
            case R.id.muac_less_then_115:
                question_metrics.setMuac_less_then_115(checked);
                if (checked == true){
                    ((CheckBox)findViewById(R.id.muac_more_then_115_and_less_then_125)).setChecked(false);
                    question_metrics.setMuac_more_then_115_and_less_then_125(false);
                }
                break;
            case R.id.muac_more_then_115_and_less_then_125:
                question_metrics.setMuac_more_then_115_and_less_then_125(checked);
                if (checked == true){
                    ((CheckBox)findViewById(R.id.muac_less_then_115)).setChecked(false);
                    question_metrics.setMuac_less_then_115(false);
                }
                break;
            case R.id.bilateral_pitting_edema_present:
                question_metrics.setBilateral_pitting_edema_present(checked);
                Integer [] id_related_to_edema = {
                        R.id.bilateral_pitting_edema_present_1plus_to_2plus,
                        R.id.bilateral_pitting_edema_present_3plus
                };
                toggleView(id_related_to_edema, checked);
                    question_metrics.setBilateral_pitting_edema_present_1plus_to_2plus(checked);
                    question_metrics.setBilateral_pitting_edema_present_3plus(checked);
                break;
            case R.id.bilateral_pitting_edema_present_1plus_to_2plus:
                question_metrics.setBilateral_pitting_edema_present_1plus_to_2plus(checked);
                if (checked == true){
                    ((CheckBox)findViewById(R.id.bilateral_pitting_edema_present_3plus)).setChecked(false);
                    question_metrics.setBilateral_pitting_edema_present_3plus(false);
                }
                break;
            case R.id.bilateral_pitting_edema_present_3plus:
                question_metrics.setBilateral_pitting_edema_present_3plus(checked);
                if (checked == true){
                    ((CheckBox)findViewById(R.id.bilateral_pitting_edema_present_1plus_to_2plus)).setChecked(false);
                    question_metrics.setBilateral_pitting_edema_present_1plus_to_2plus(false);

                }
                break;
            case R.id.wt_for_height_less_then_minus_3sd:
                question_metrics.setWt_for_height_less_then_minus_3sd(checked);
                if (checked == true){
                    ((CheckBox)findViewById(R.id.wt_for_height_minus_2sd_to_minus_3sd)).setChecked(false);
                    question_metrics.setWt_for_height_minus_2sd_to_minus_3sd(false);

                }
                break;
            case R.id.wt_for_height_minus_2sd_to_minus_3sd:
                question_metrics.setWt_for_height_minus_2sd_to_minus_3sd(checked);
                if (checked == true){
                    ((CheckBox)findViewById(R.id.wt_for_height_less_then_minus_3sd)).setChecked(false);
                    question_metrics.setWt_for_height_less_then_minus_3sd(false);

                }
                break;
            case R.id.not_eat_rutf:
                question_metrics.setNot_eat_rutf(checked);
                if (checked == true){
                    ((CheckBox)findViewById(R.id.demonstrate_appitie)).setChecked(false);
                    question_metrics.setDemonstrate_appitie(false);

                }
                break;
            case R.id.demonstrate_appitie:
                question_metrics.setDemonstrate_appitie(checked);
                if (checked == true){
                    ((CheckBox)findViewById(R.id.not_eat_rutf)).setChecked(false);
                    question_metrics.setNot_eat_rutf(false);

                }
                break;
            //medical complication
            case R.id.intractable_vomiting:
                question_metrics.setIntractable_vomiting(checked);
                break;
            case R.id.fever:
                question_metrics.setFever(checked);
                if (checked == true){
                    ((CheckBox)findViewById(R.id.hypothermia)).setChecked(false);
                    question_metrics.setHypothermia(false);

                }
                break;
            case R.id.hypothermia:
                question_metrics.setHypothermia(checked);
                if (checked == true){
                    ((CheckBox)findViewById(R.id.fever)).setChecked(false);
                    question_metrics.setFever(false);
                }
                break;
            case R.id.lower_respiratory_tract_infection:
                question_metrics.setLower_respiratory_tract_infection(checked);
                break;

            case R.id.severe_anaemia:
                question_metrics.setSevere_anaemia(checked);
                break;
            case R.id.not_alert:
                question_metrics.setNot_alert(checked);
                break;

            case R.id.jaundice_complication:
                question_metrics.setJaundice_complication(checked);
                break;
            case R.id.superficial_infection:
                question_metrics.setSuperficial_infection(checked);
                break;
            case R.id.sever_dehydration:
                question_metrics.setSever_dehydration(checked);
                break;
            case R.id.eye_infection_and_other_eye_problem:
                question_metrics.setEye_infection_and_other_eye_problem(checked);
                break;

        }
    }

    private void toggleView(Integer [] viewsIds, Boolean checked ){
        for(int i = 0 ; i < viewsIds.length; i++){
            if (checked){
                findViewById(viewsIds[i]).setVisibility(View.VISIBLE);
            }
            else{
                findViewById(viewsIds[i]).setVisibility(View.GONE);
            }
        }
    }

    public void updateViews(){
        recreate();
    }

}
