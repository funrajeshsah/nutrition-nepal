package com.nepget.rhino;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

public class WelcomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LocaleHelper.onCreate(this);
        setContentView(R.layout.activity_welcome);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    public void onClickAge(View view){
        String age_group = "";
        Intent intent = null;
        Log.v("sendMsg", "before intent");
        if ((view.getId() == R.id.radioButtonLessThen2Month)){
            intent = new Intent(this, LessThen2MonthActivity.class);
        }else {
            intent = new Intent(this, MoreThen2MonthActivity.class);
        }

        intent.putExtra("question_metrics", age_group);
        startActivity(intent);
    }
}
