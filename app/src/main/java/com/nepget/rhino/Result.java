package com.nepget.rhino;

import java.util.ArrayList;

/**
 * Created by rajesh on 28/6/16.
 */
public class Result {

    private String classify = "";
    private String severity = "GREEN";
    private String dosages = "";
    private Integer treatments = R.string.treatments;
    private ArrayList<String> signs = new ArrayList<String>(10);


    public String getClassify() {
        return classify;
    }

    public void setClassify(String classify) {
        this.classify = classify;
    }

    public String getSeverity() {
        return severity;
    }

    public void setSeverity(String severity) {
        this.severity = severity;
    }



    public ArrayList<String> getSigns() {
        return signs;
    }

    public void setSigns(ArrayList<String> signs) {
        this.signs = signs;
    }

    public String getDosages() {
        return dosages;
    }

    public void setDosages(String dosages) {
        this.dosages = dosages;
    }

    public Integer getTreatments() {
        return treatments;
    }

    public void setTreatments(Integer treatments) {
        this.treatments = treatments;
    }

    public Result(String classify, String severity, String dosages, Integer treatments, ArrayList<String> signs) {
        this.classify = classify;
        this.severity = severity;
        this.dosages = dosages;
        this.treatments = treatments;
        this.signs = signs;
    }

    public String signsToString(){
        String signs_string = "";
        for(String sign :signs ){
            signs_string = signs_string +"-> " + sign;
        }
        return signs_string;
    }



    @Override
    public String toString() {
        return "Result{" +
                "classify='" + classify + '\'' +
                ", severity='" + severity + '\'' +
                ", treatments=" + treatments +
                ", signs=" + signsToString() +
                '}';
    }
}
