package com.nepget.rhino;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;

import com.nepget.rhino.db.DB_Controller;
import com.nepget.rhino.utility.Data;
import com.nepget.rhino.utility.DataSelectedKey;
import com.nepget.rhino.utility.FieldNameConstantMap;
import com.nepget.rhino.utility.SelectedCheckboxData;

import java.util.ArrayList;
import java.util.List;

public class DisplayMessageActivity extends BaseActivity {

    private List<ResultEntity> resultEntitiesList = new ArrayList<>();
    private RecyclerView mRecyclerView;
    private ResultAdapter mAdapter;
    DB_Controller db_controller ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        super.onCreateDrawer();
        LocaleHelper.onCreate(this);

        FrameLayout frame = (FrameLayout) findViewById(R.id.containt_framme);
        View childView =  getLayoutInflater().inflate(R.layout.activity_display_message,null);
        frame.addView(childView);


        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        ArrayList<Result> results = new ArrayList<Result>(0);

        mAdapter = new ResultAdapter(resultEntitiesList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdapter);
        Intent intent = getIntent();
        Data question_metrics = (Data)intent.getSerializableExtra("question_metrics");
        String is_lessThen2 = (String)intent.getStringExtra("lessThen2");
        if (is_lessThen2.equalsIgnoreCase("true")){
            results = LessThen2MonthTreatmentCalculator.process(question_metrics);
        }else{
            results = TreatmentCalculator.process(question_metrics);
        }

        List<SelectedCheckboxData> select_checkbox = DataSelectedKey.getSelectedCheckbox(question_metrics);
        /*String listString = "";

        for (Integer s : select_checkbox)
        {
            listString += getString(s);
        }
        Log.d("SELECTED_CHECKBOX", listString);*/

       /* db_controller = new DB_Controller(this,"",null,3);
        db_controller.insertAppointment(getClassifiedConstants(results),"123",getSignConstants(question_metrics));
        db_controller.getAllAppointment();*/
        /*AsyncT asyncT = new AsyncT();
        asyncT.execute();*/

       prepareMovieData(results);
    }

    private void prepareMovieData(ArrayList<Result> results) {
        for(Result result: results){

            resultEntitiesList.add(new ResultEntity(getStringResourceByName(result.getClassify()), result.getSeverity(), result.signsToString(), result.getDosages()
                    , result.getTreatments()));
        }

        mAdapter.notifyDataSetChanged();
    }

    private String getClassifiedConstants(List<Result> results){
        String result= "";
        for(Result r :results){
            result = result+ "," + FieldNameConstantMap.getFieldNameConstantMap().get(r.getClassify());
        }
        return result;

    }

    private String getSignConstants(Data question_metrics){
        String result= "";
        List<SelectedCheckboxData> select_checkbox = DataSelectedKey.getSelectedCheckbox(question_metrics);
        for(SelectedCheckboxData r :select_checkbox){
            result = result+ "," + FieldNameConstantMap.getFieldNameConstantMap().get(r.getName());
        }
        return result;

    }

    private Integer getStringResourceByName(String aString) {
        String packageName = getPackageName();
        int resId = getResources().getIdentifier(aString, "string", packageName);
        return resId;
    }

}
