package com.nepget.rhino;

import android.util.Log;

import com.nepget.rhino.utility.Data;

import java.util.ArrayList;

/**
 * Created by rajesh on 28/6/16.
 */
public class TreatmentCalculator {

    private static final String TAG = "TreatmentCalculator";
    public static boolean generalDangerSign = false;
    public static boolean dehydrationPresent = false;
    private static String dosages = "";

    public static ArrayList<Result> process(Data data) {
        generalDangerSign = false;
        dehydrationPresent = false;
        ArrayList<Result> results = new ArrayList<Result>(10);
        classifyIMAM(data, results);
        return results;
    }

    private static void classifyIMAM(Data data, ArrayList<Result> results) {

        String severity = "White";
        String classify = "";
        ArrayList<String> signs = new ArrayList<String>(10);
        Integer treatments = 1;

        //check if it is SAM or MAM
        if(data.getMuac_less_then_115() || data.getBilateral_pitting_edema_present() || data.getWt_for_height_less_then_minus_3sd()  ) {
            //SAM with complication
            if (isMedicationComplication(data) ||
                    data.getNot_eat_rutf() ||
                    data.getBilateral_pitting_edema_present_3plus() ||
                    (data.getMuac_less_then_115()  && data.getBilateral_pitting_edema_present_1plus_to_2plus())
                    ) {
                severity = "White";
                classify = "classfy_SAM_with_complication";
                treatments = R.string.treatment_SAM_with_complication;
            }
            else {
                severity = "White";
                classify = "classfy_SAM_with_out_complication";
                treatments = R.string.treatment_SAM_with_out_complication;
            }

        }else if(data.getMuac_more_then_115_and_less_then_125() || data.getWt_for_height_minus_2sd_to_minus_3sd()){
            if(!data.getBilateral_pitting_edema_present() ){
                severity = "White";
                classify = "classfy_MAM";
                treatments = R.string.treatment_MAM;
                if(isMedicationComplication(data)){
                    severity = "White";
                    classify = "classfy_MAM_with_complication";
                    treatments = R.string.treatment_MAM_with_complication;
                }
            }else {
                severity = "White";
                classify = "classify_No_signs_SAM_or_MAM";
                signs.add("No signs SAM or MAM");
            }

            treatments = R.string.treatment_No_signs_SAM_or_MAM;
        }else{


                severity = "White";
                classify = "classify_No_signs_SAM_or_MAM";
                signs.add("No signs SAM or MAM");

                treatments = R.string.treatment_No_signs_SAM_or_MAM;


        }

        Result result = new Result(classify, severity, dosages,treatments, signs);
        results.add(result);

        /*//Any general danger sign
        if (data.getCoughOrDifficultBreathing()) {
            if (generalDangerSign || data.getStridor()) {
                generalDangerSign = true;
                severity = "Pink";
                classify = "classfy_SEVERE_PNEUMONIA_OR_VERY_SEVERE_DISEASE";
                if (data.getStridor()) {
                    signs.add("Stridor or general danger ");
                }
                if (generalDangerSign) {
                    signs.add("General danger");
                }
                treatments = R.string.treatment_SEVERE_PNEUMONIA_OR_VERY_SEVERE_DISEASE;
            } else if (data.getChestIndrawing() || data.getFastBreathing()) {
                severity = "Yellow";
                classify = "classfy_PNEUMONIA";
                treatments = R.string.treatment_PNEUMONIA;
                dosages = "Amoxicillin";
            } else {

                severity = "Green";
                classify = "classfy_cough_or_cold";
                signs.add("No signs of pneumonia or\n" +
                        "very severe disease.");

                treatments = R.string.treatment_cough_or_cold;

            }

            Result result = new Result(classify, severity, dosages,treatments, signs);
            results.add(result);

        }
*/
    }

    private static boolean isMedicationComplication(Data data) {
        return (
                data.getIntractable_vomiting() ||
                data.getFever() ||
                data.getHypothermia() ||
                data.getLower_respiratory_tract_infection() ||
                data.getSevere_anaemia() ||
                data.getNot_alert() ||
                data.getJaundice_complication() ||
                data.getSuperficial_infection() ||
                data.getSever_dehydration() ||
                data.getEye_infection_and_other_eye_problem()
        );
    }


    private static int booleanToInt(Boolean value) {
        if (value) {
            return 1;
        } else {
            return 0;
        }
    }


}
