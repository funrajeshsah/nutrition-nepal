package com.nepget.rhino;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ExpandableListView.OnGroupCollapseListener;
import android.widget.ExpandableListView.OnGroupExpandListener;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.nepget.rhino.utility.Booklet;
import com.nepget.rhino.utility.Headings;
import com.nepget.rhino.utility.Subheading;
import com.nepget.rhino.utility.Treatment;

public class DExpandableActivity extends AppCompatActivity {

    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    Booklet booklet = new Booklet();
    Treatment doase = new Treatment();

    private String TAG = "DExpandableActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dexpandable);

        Intent intent = getIntent();
        String item_name  = (String)intent.getStringExtra("item_name");
        if(item_name.equalsIgnoreCase("book_chart")){
            Log.d(TAG, "book_chart: called" );
            updateViews(ServerData.getInstance().getBooklet());
        }else{
            Log.d(TAG, "getDoase: called" );
            updateViews(ServerData.getInstance().getDoase());
        }
    }


    //start
    private void prepareListData(Booklet booklet) {
        Log.d(TAG, "booklet: " + booklet );
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        for(Headings heading: booklet.getHeadings()){
            listDataHeader.add(heading.getText());
            List<String> subHeadings = new ArrayList<String>();
            for(Subheading subheading: heading.getSubheading()){
                subHeadings.add(subheading.getText());
            }
            listDataChild.put(heading.getText(), subHeadings);
        }

    }

    //function for dose
    private void prepareListDataDose(Treatment dose) {
        Log.d(TAG, "Treatment: " + dose );
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        for(Headings heading: dose.getHeadings()){
            listDataHeader.add(heading.getText());
            List<String> subHeadings = new ArrayList<String>();
            for(Subheading subheading: heading.getSubheading()){
                subHeadings.add(subheading.getText());
            }
            listDataChild.put(heading.getText(), subHeadings);
        }
    }
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, Home.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

    }

    public void updateViews(Object booklet){
        expListView = (ExpandableListView) findViewById(R.id.lvExp);

        Intent intent = getIntent();
        String item_name  = (String)intent.getStringExtra("item_name");
        try {
            if(item_name.equalsIgnoreCase("book_chart")){
                // preparing list data
                prepareListData(Booklet.class.cast(booklet));
            }else{
                prepareListDataDose(Treatment.class.cast(booklet));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        listAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild);

        // setting list adapter
        expListView.setAdapter(listAdapter);

        // Listview Group click listener
        expListView.setOnGroupClickListener(new OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                // Toast.makeText(getApplicationContext(),
                // "Group Clicked " + listDataHeader.get(groupPosition),
                // Toast.LENGTH_SHORT).show();
                return false;
            }
        });

        // Listview Group expanded listener
        expListView.setOnGroupExpandListener(new OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                String headerString = listDataHeader.get(groupPosition);
                Log.v(TAG, headerString);
                if(headerString.contains("no_sub")){
                    String file_name= headerString.replace("_header_no_sub","");
                    Log.v(TAG, headerString);
                    Intent  intent = new Intent(getApplicationContext(), NavResult.class);
                    intent.putExtra("item_name", file_name);
                    startActivity(intent);
                }

            }
        });

        // Listview Group collasped listener
        expListView.setOnGroupCollapseListener(new OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
                String headerString = listDataHeader.get(groupPosition);
                Log.v(TAG, headerString);
                if(headerString.contains("no_sub")){
                    String file_name= headerString.replace("_header_no_sub","");
                    Log.v(TAG, headerString);
                    Intent  intent = new Intent(getApplicationContext(), NavResult.class);
                    intent.putExtra("item_name", file_name);
                    startActivity(intent);
                }
            }
        });

        // Listview on child click listener
        expListView.setOnChildClickListener(new OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                Log.v(TAG, "before browse_content  intent");
                Intent oldIntent = getIntent();
                String source  = (String)oldIntent.getStringExtra("item_name");

                Intent  intent = new Intent(getApplicationContext(), NavResult.class);
                intent.putExtra("source",source );


                String heading = listDataHeader.get(groupPosition);
                String subHeading = listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition);
                String headingPlusSubHeading = heading+"__"+subHeading;
                intent.putExtra("item_name", headingPlusSubHeading);
                startActivity(intent);
                return false;
            }
        });
    }


}