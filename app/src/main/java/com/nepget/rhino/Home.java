package com.nepget.rhino;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class Home extends BaseActivity{

    private String TAG = "Home";
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LocaleHelper.onCreate(this);
        //setContentView(R.layout.content_home_select_age);

        // Initialize Firebase Auth
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();

        if (mFirebaseUser == null) {
            // Not logged in, launch the Log In activity
            loadLogInView();
        }

        setContentView(R.layout.activity_home);
        super.onCreateDrawer();
        FrameLayout frame = (FrameLayout) findViewById(R.id.containt_framme);
        View childView =  getLayoutInflater().inflate(R.layout.content_home_select_age,null);
        frame.addView(childView);
    }

    public void onClickAge(View view){
        String age_group = "";
        Intent intent = null;
        Log.v("sendMsg", "before intent");
        if ((view.getId() == R.id.radioButtonLessThen2Month)){
            intent = new Intent(this, LessThen2MonthActivity.class);
        }else if ((view.getId() == R.id.browse_content)){
            Log.v("sendMsg", "before browse_content intent");
            intent = new Intent(this, DExpandableActivity.class);
            intent.putExtra("item_name", "book_chart");
            startActivity(intent);
        }
        else if ((view.getId() == R.id.dosage_content)){
            Log.v("sendMsg", "before browse_content intent");
            intent = new Intent(this, DExpandableActivity.class);
            intent.putExtra("item_name", "dosage_content");
            startActivity(intent);
        }
        else {
            intent = new Intent(this, MoreThen2MonthActivity.class);
        }


        intent.putExtra("question_metrics", age_group);
        startActivity(intent);
    }

    //back button on home will exit the app
    /*@Override
    public void onBackPressed() {

            new AlertDialog.Builder(this)
                    .setMessage("Are you sure you want to exit?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                           // Log.v("onBackPressed", "exit from app");
                            moveTaskToBack(true);
                            finish();
                            System.exit(0);
                        }

                    })
                    .setNegativeButton("No", null)
                    .show();
        }
*/

    private void loadLogInView() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

}
