package com.nepget.rhino;

import android.content.Context;
import android.content.Intent;
import android.text.Layout;
import android.text.method.LinkMovementMethod;
import android.text.style.URLSpan;
import android.util.Log;
import android.view.MotionEvent;

/**
 * Created by rajesh on 13/7/16.
 */
public class CustomLinkMovementMethod extends LinkMovementMethod {

    private static Context movementContext;

    private static CustomLinkMovementMethod linkMovementMethod = new CustomLinkMovementMethod();

    public boolean onTouchEvent(android.widget.TextView widget, android.text.Spannable buffer, android.view.MotionEvent event) {
        int action = event.getAction();
        Log.d("CustomLink", "CustomLinkMovementMethod");
        if (action == MotionEvent.ACTION_UP) {
            int x = (int) event.getX();
            int y = (int) event.getY();
            x -= widget.getTotalPaddingLeft();
            y -= widget.getTotalPaddingTop();

            x += widget.getScrollX();
            y += widget.getScrollY();

            Layout layout = widget.getLayout();
            int line = layout.getLineForVertical(y);
            int off = layout.getOffsetForHorizontal(line, x);

            URLSpan[] link = buffer.getSpans(off, off, URLSpan.class);
            if (link.length != 0) {
                Log.d("Link total", link.toString());
                String url = link[0].getURL();
                //if (url.startsWith("http")) {
                    Log.d("Link", url);
                    //Toast.makeText(movementContext, "Link was clicked", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(movementContext, Dosages.class);
                    intent.putExtra("dosages_name", url);
                    movementContext.startActivity(intent);
                //}
                return true;
            }
        }

        return super.onTouchEvent(widget, buffer, event);
    }

    public static android.text.method.MovementMethod getInstance(Context c)
    {
        movementContext = c;
        return linkMovementMethod;
    }
}

    /*public static android.text.method.MovementMethod getInstance(Context c)
    {
        movementContext = c;
        return linkMovementMethod;
    }*/