package com.nepget.rhino;

import android.app.Application;
import android.util.Log;

/**
 * Created by rajesh on 14/7/16.
 */
public class MainApplication extends Application {
    String TAG = "MainApplication";
    private static  String selected_langauge_value = "hi";

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "in MainApplication");
        //if (LocaleHelper.isFirstTime(this)){
            LocaleHelper.onCreate(this, selected_langauge_value);
        Log.d(TAG, "in MainApplication, selected_langauge_value : " +selected_langauge_value);
        //}
        ServerData.getInstance();

    }




}
