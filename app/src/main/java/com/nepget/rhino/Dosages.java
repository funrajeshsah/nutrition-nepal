package com.nepget.rhino;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;

import java.util.ArrayList;

public class Dosages extends BaseActivity {
    private static String TAG = "Dosages";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        super.onCreateDrawer();
        LocaleHelper.onCreate(this);

        FrameLayout frame = (FrameLayout) findViewById(R.id.containt_framme);
        View childView =  getLayoutInflater().inflate(R.layout.activity_dosages,null);
        frame.addView(childView);

        Intent intent = getIntent();
        String dosages_name  = (String)intent.getStringExtra("dosages_name");
        ArrayList<Integer> image_ids = new ArrayList<>(5);
        if(dosages_name != null){
            if(dosages_name.contains("diazepam")){
                displayWebView("file:///android_asset/www/dosage/diazepam_dose_2to5.html");
            }
            if (dosages_name.contains("Amoxicillin_dose_0to2")){
                displayWebView("file:///android_asset/www/dosage/Amoxicillin_dose_0to2.html");
            }
            if (dosages_name.contains("Amoxicillin_2to5")){
                displayWebView("file:///android_asset/www/dosage/Amoxicillin_dose_2to5.html");
            }
            if (dosages_name.contains("Ampicillin_dose_0to2")){
                displayWebView("file:///android_asset/www/dosage/Ampicillin_dose_0to2.html");
            }
            if (dosages_name.contains("ciprofloxacin_azithromycin_dose_2to5")){
                displayWebView("file:///android_asset/www/dosage/ciprofloxacin_azithromycin_dose_2to5.html");
            }
            if (dosages_name.contains("Ciprofloxacin")){
                displayWebView("file:///android_asset/www/dosage/Ciprofloxacin_dose_0to6_6to5.html");
            }
            if (dosages_name.contains("cotrimoxazole_dose_0to6_6to5")){
                displayWebView("file:///android_asset/www/dosage/cotrimoxazole_dose_0to6_6to5.html");
            }
            if (dosages_name.contains("Gentamicin")){
                displayWebView("file:///android_asset/www/dosage/Gentamicin_dose_0to2.html");
            }
            if (dosages_name.contains("Iron")){
                displayWebView("file:///android_asset/www/dosage/Iron_dose_2to5.html");
            }
            if (dosages_name.contains("paracetamol")){
                displayWebView("file:///android_asset/www/dosage/paracetamol_2to5.html");
            }
            if (dosages_name.contains("salbutamol")){
                displayWebView("file:///android_asset/www/dosage/salbutamol_dose_2to5.html");
            }
            if (dosages_name.contains("vitamin_A")){
                displayWebView("file:///android_asset/www/dosage/vitamin_A_6to5.html");
            }
            if (dosages_name.contains("local_bacterial_home_treatment_0to2")){
                displayWebView("file:///android_asset/www/dosage/local_bacterial_home_treatment_0to2.html");
            }
            if (dosages_name.contains("jundice_home_treatment_0to2")){
                displayWebView("file:///android_asset/www/dosage/jundice_home_treatment_0to2.html");
            }
            if (dosages_name.contains("possible_sever_bacterial_infection")){
                displayWebView("file:///android_asset/www/dosage/possible_sever_bacterial_infection.html");
            }
            if (dosages_name.contains("breast_feeding_technique")){
                displayWebView("file:///android_asset/www/dosage/breast_feeding_technique.html");
            }
            if (dosages_name.contains("oral_thrush")){
                displayWebView("file:///android_asset/www/dosage/oral_thrush.html");
            }
            if (dosages_name.contains("Antibiotics")) {
                displayWebView("file:///android_asset/www/dosage/Antibiotics.html");
            }
            if (dosages_name.contains("Antibiotics_ampicillin")) {
                displayWebView("file:///android_asset/www/dosage/Antibiotics_ampicillin.html");
            }
            if (dosages_name.contains("med_for_neck_cough")) {
                displayWebView("file:///android_asset/www/dosage/med_for_neck_cough.html");
            }
            if (dosages_name.contains("parnali_ga")) {
                displayWebView("file:///android_asset/www/dosage/parnali_ga.html");
            }
            if (dosages_name.contains("Antibiotic_for_cholera")) {
                displayWebView("file:///android_asset/www/dosage/Antibiotic_for_cholera.html");
            }
            if (dosages_name.contains("parnali_kha")) {
                displayWebView("file:///android_asset/www/dosage/parnali_kha.html");
            }
            if (dosages_name.contains("zinc_tab_dehydration")) {
                displayWebView("file:///android_asset/www/dosage/zinc_tab_dehydration.html");
            }
            if (dosages_name.contains("parnali_ka")) {
                displayWebView("file:///android_asset/www/dosage/parnali_ka.html");
            }
            if (dosages_name.contains("Rectal_Artisunate")) {
                displayWebView("file:///android_asset/www/dosage/Rectal_Artisunate.html");
            }
            if (dosages_name.contains("Artemisinin_Combination_Therapy")) {
                displayWebView("file:///android_asset/www/dosage/Artemisinin_Combination_Therapy.html");
            }
            if (dosages_name.contains("Chloroquine_dose_agegroup")) {
                displayWebView("file:///android_asset/www/dosage/Chloroquine_dose_agegroup.html");
            }
            if (dosages_name.contains("Tetracycline_ointment")) {
                displayWebView("file:///android_asset/www/dosage/Tetracycline_ointment.html");

            }
            if (dosages_name.contains("gentian_violet")) {
                displayWebView("file:///android_asset/www/dosage/gentian_violet.html");

            }
            if (dosages_name.contains("Ciprofloxacin_drop")) {
                displayWebView("file:///android_asset/www/dosage/Ciprofloxacin_drop.html");

            }
            if (dosages_name.contains("Albendazole")) {
                displayWebView("file:///android_asset/www/dosage/Albendazole.html");

            }
            if (dosages_name.contains("hypoglycaemia")) {
                displayWebView("file:///android_asset/www/dosage/hypoglycaemia.html");

            }


            if (dosages_name.contains("parnali_kha_lessthan2month")) {
                displayWebView("file:///android_asset/www/dosage/parnali_kha_lessthan2month.html");
            }

            if (dosages_name.contains("parnali_ka_lessthan2month")) {
                displayWebView("file:///android_asset/www/dosage/parnali_ka_lessthan2month.html");
            }else {
                String html_content = "No content found";
                dosages_name = dosages_name.replaceAll("_"," ");
                html_content = ServerData.getInstance().getBooklet().getSubheadingMap().get(dosages_name).getHtml();
                displayWebView(html_content,true);
            }
        }


        /*for(int i=0; i < image_ids.size(); i++){
            if(i== 0){
                HorizontalScrollView dosageScrollView1 = (HorizontalScrollView) findViewById(R.id.dosageScrollView1);
                dosageScrollView1.setVisibility(View.VISIBLE);
                ImageView image = (ImageView) findViewById(R.id.dosageImage1);
                image.setImageResource(image_ids.get(i));
            }
            if(i== 1){
                HorizontalScrollView dosageScrollView1 = (HorizontalScrollView) findViewById(R.id.dosageScrollView2);
                dosageScrollView1.setVisibility(View.VISIBLE);
                ImageView image = (ImageView) findViewById(R.id.dosageImage2);
                image.setImageResource(image_ids.get(i));
            }
            if(i== 2){
                HorizontalScrollView dosageScrollView1 = (HorizontalScrollView) findViewById(R.id.dosageScrollView3);
                dosageScrollView1.setVisibility(View.VISIBLE);
                ImageView image = (ImageView) findViewById(R.id.dosageImage3);
                image.setImageResource(image_ids.get(i));
            }
        }
        //,quinine,pracetamol
        *//*ImageView image = (ImageView) findViewById(R.id.nav_result_image);
        if(item_name.equalsIgnoreCase("nav_principle_of_imnci")){
            image.setImageResource(R.drawable.principles_of_cbimnci);
        }*//**//*else if(item_name.equalsIgnoreCase("hand_washing_technique")){
            image.setImageResource(R.drawable.general_danger);
        }*//**//*
        else if(item_name.equalsIgnoreCase("feeding_counselling")){
            image.setImageResource(R.drawable.feeding_counselling);
        }
        else if(item_name.equalsIgnoreCase("when_to_return_immediately")){
            image.setImageResource(R.drawable.whenreturn);
        }
        else if(item_name.equalsIgnoreCase("good_home_care_for_child")){
            image.setImageResource(R.drawable.homecare);
        }*/
    }

    private  void displayWebView(String html_path){
        WebView webview = (WebView) findViewById(R.id.webViewDosage);
        webview.setWebViewClient(new MyAppWebViewClient());
        webview.setVisibility(View.VISIBLE);
        WebSettings webSettings = webview.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setBuiltInZoomControls(true);
        webview.loadUrl(html_path);
    }

    private  void displayWebView(String html_content, boolean isHtml){
        WebView webview = (WebView) findViewById(R.id.webViewDosage);
        webview.setWebViewClient(new MyAppWebViewClient());
        webview.setVisibility(View.VISIBLE);
        WebSettings webSettings = webview.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setBuiltInZoomControls(true);
        webview.loadData(html_content, "text/html", "UTF-8");
    }

    class MyAppWebViewClient extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            //if(Uri.parse(url).getHost().endsWith("html5rocks.com")) {
            return false;
            //}
        }
    }
}
