package com.nepget.rhino;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

/**
 * Created by rajesh on 29/6/16.
 */
public class ResultAdapter extends RecyclerView.Adapter<ResultAdapter.ResultViewHolder> {
    private List<ResultEntity> resultList;

    public class ResultViewHolder extends RecyclerView.ViewHolder {
        public TextView classify_case, result_temp, symptoms;
        public RelativeLayout card_relative_layout;
        public Button dosages;

        public ResultViewHolder(View view) {
            super(view);
            classify_case = (TextView) view.findViewById(R.id.classify_case);
            result_temp = (TextView) view.findViewById(R.id.result_temp);
            symptoms = (TextView) view.findViewById(R.id.symptoms);
            dosages = (Button) view.findViewById(R.id.button_dosage);
            card_relative_layout = (RelativeLayout) view.findViewById(R.id.card_relative_layout);
        }
    }


    public ResultAdapter(List<ResultEntity> resultList) {
        this.resultList = resultList;
    }

    @Override
    public ResultViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.result_list_row, parent, false);
        return new ResultViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ResultViewHolder holder, int position) {
        ResultEntity resultEntity = resultList.get(position);
        holder.classify_case.setText(resultEntity.getClassify());
        holder.result_temp.setText(Html.fromHtml(holder.result_temp.getContext().getResources().getString(resultEntity.getTreatments())));
        holder.result_temp.setMovementMethod(CustomLinkMovementMethod.getInstance(holder.result_temp.getContext()));
        holder.symptoms.setText(resultEntity.getSigns());
        holder.dosages.setOnClickListener(new DosagesOnClickListener(resultEntity.getDosages()));

        if (resultEntity.getSeverity().equalsIgnoreCase("Pink")){
            holder.card_relative_layout.setBackgroundColor(Color.argb(255,250,208,233));
        }
        else if(resultEntity.getSeverity().equalsIgnoreCase("Yellow")){
            holder.card_relative_layout.setBackgroundColor(Color.argb(255,247,247,38));
        }
        else if(resultEntity.getSeverity().equalsIgnoreCase("Green")){
            holder.card_relative_layout.setBackgroundColor(Color.argb(255,140,250,163));
        }
        else if(resultEntity.getSeverity().equalsIgnoreCase("White")){
            //holder.card_relative_layout.setBackgroundColor(Color.argb(255,140,250,163));
        }
    }

    @Override
    public int getItemCount() {
        return resultList.size();
    }
}

class DosagesOnClickListener implements View.OnClickListener
{

    String dosages_name = "";
    public DosagesOnClickListener(String dosages_name) {
        this.dosages_name = dosages_name;
    }

    @Override
    public void onClick(View v)
    {
      //  Toast.makeText(v.getContext(),dosages_name ,
      //          Toast.LENGTH_LONG).show();
        Intent intent = new Intent(v.getContext(), Dosages.class);
        intent.putExtra("dosages_name", dosages_name);
        v.getContext().startActivity(intent);
    }

}
