package com.nepget.rhino;

/**
 * Created by rajesh on 29/6/16.
 */
public class ResultEntity {
    private Integer classify = R.string.classify_str;
    private String severity = "GREEN";

    private String signs = "";
    private String dosages = "";
    private Integer treatments = R.string.treatments;;

    public Integer getClassify() {
        return classify;
    }

    public void setClassify(Integer classify) {
        this.classify = classify;
    }

    public String getSeverity() {
        return severity;
    }

    public void setSeverity(String severity) {
        this.severity = severity;
    }


    public String getSigns() {
        return signs;
    }

    public void setSigns(String signs) {
        this.signs = signs;
    }

    public String getDosages() {
        return dosages;
    }

    public void setDosages(String dosages) {
        this.dosages = dosages;
    }

    public Integer getTreatments() {
        return treatments;
    }

    public void setTreatments(Integer treatments) {
        this.treatments = treatments;
    }

    public ResultEntity(Integer classify, String severity, String signs, String dosages, Integer treatments) {
        this.classify = classify;
        this.severity = severity;
        this.signs = signs;
        this.dosages = dosages;
        this.treatments = treatments;
    }

    public ResultEntity(){

    }
}
