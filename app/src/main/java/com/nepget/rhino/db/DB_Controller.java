package com.nepget.rhino.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by rajesh on 28/7/16.
 */
public class DB_Controller extends SQLiteOpenHelper {


    private static final String TABLE_NAME = "APPOINTMENTS";
    private static final String COLUMN_CLASSIFIED_NAME = "CLASSIFIEDHEADER";
    private static final String COLUMN_APPOINTMENT_ID_NAME = "APPOINTMENTID";
    private static final String COLUMN_SIGNS_NAME = "SIGNS";
    private static final String COLUMN_SYNC_NAME = "SYNC";



    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + TABLE_NAME;
    public DB_Controller(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, "imnci.db", factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE "+ TABLE_NAME+ "( ID INTEGER PRIMARY KEY AUTOINCREMENT, "+COLUMN_CLASSIFIED_NAME +" TEXT, "+COLUMN_APPOINTMENT_ID_NAME +" TEXT, "+COLUMN_SIGNS_NAME +" TEXT, "+COLUMN_SYNC_NAME+" TEXT);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(SQL_DELETE_ENTRIES);
        onCreate(sqLiteDatabase);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public void insertAppointment(String classifiedTitle, String appointment_id, String signs){
        ContentValues contentValues = new ContentValues();
        contentValues.put(DB_Controller.COLUMN_CLASSIFIED_NAME, classifiedTitle);
        contentValues.put(DB_Controller.COLUMN_APPOINTMENT_ID_NAME, appointment_id);
        contentValues.put(DB_Controller.COLUMN_SIGNS_NAME, signs);
        contentValues.put(DB_Controller.COLUMN_SYNC_NAME, false);
        this.getWritableDatabase().insertOrThrow(DB_Controller.TABLE_NAME, "", contentValues);
    }

    public void getAllAppointment(){
        Cursor cursor = this.getReadableDatabase().rawQuery("SELECT * FROM "+DB_Controller.TABLE_NAME, null);
        while (cursor.moveToNext()){
            Log.d("TESTLOG",cursor.getString(1)+" : " + cursor.getString(2)+" : " + cursor.getString(3)+" : " + cursor.getString(4));
        }
    }

}
