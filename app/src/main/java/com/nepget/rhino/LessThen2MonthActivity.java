package com.nepget.rhino;

import android.content.Intent;
import android.graphics.Matrix;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.webkit.WebView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;

import com.nepget.rhino.utility.Data;

public class LessThen2MonthActivity extends BaseActivity {

    public static final String TAG = "LessThen2MonthActivity";
    public static Data question_metrics;
    ScaleGestureDetector scaleGestureDetector;
    Matrix matrix = new Matrix();
    TouchImageView image;
    WebView webView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LocaleHelper.onCreate(this);
        setContentView(R.layout.activity_home);
        super.onCreateDrawer();
        FrameLayout frame = (FrameLayout) findViewById(R.id.containt_framme);
        View childView =  getLayoutInflater().inflate(R.layout.content_less_then2_month,null);
        frame.addView(childView);
        question_metrics = new Data();

    }




    public void sendMessage(View view){

        Log.v("sendMsg", "before intent");
        Intent intent = new Intent(this, DisplayMessageActivity.class);
        Log.v("sendMsg", "before intent");
        intent.putExtra("lessThen2", "true");
        intent.putExtra("question_metrics", question_metrics);
        Log.v("sendMsg", "after intent");
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.nepali_language) {
            LocaleHelper.setLocale(this, "en");
            updateViews();
            return true;
        }
        /*else if (id == R.id.english_language) {
            Log.d(TAG, "inside settings english");
            LocaleHelper.setLocale(this, "en");
            updateViews();
            return true;
        }*/
        return super.onOptionsItemSelected(item);
    }


    public void helpDetail(View view){
        String filePath = "";
        switch(view.getId()) {
            case R.id.help_med_complication:
                filePath = "file:///android_res/raw/help_med_complication.html";
                break;
        }

        if (!filePath.equalsIgnoreCase("")){
            webView = new WebView(this);
            webView.loadUrl(filePath);
            webView.getSettings().setUseWideViewPort(true);
            AlertDialog.Builder alert1 = new AlertDialog.Builder(this);
            alert1.setView(webView);
            alert1.show();
        }

    }


    public void onCheckboxClicked(View view) {



        // Is the view now checked?
        boolean checked = ((CheckBox) view).isChecked();

        // Check which checkbox was clicked
        switch(view.getId()) {
            case R.id.muac_less_then_21_cm:
                question_metrics.setMuac_less_then_21_cm(checked);
                if (checked == true){
                    ((CheckBox)findViewById(R.id.muac_more_then_21cm_and_less_then_23cm)).setChecked(false);
                    question_metrics.setMuac_more_then_21cm_and_less_then_23cm(false);
                }
                break;
            case R.id.muac_more_then_21cm_and_less_then_23cm:
                question_metrics.setMuac_more_then_21cm_and_less_then_23cm(checked);
                if (checked == true){
                    ((CheckBox)findViewById(R.id.muac_less_then_21_cm)).setChecked(false);
                    question_metrics.setMuac_less_then_21_cm(false);
                }
                break;
        }
    }

    private void toggleView(Integer [] viewsIds, Boolean checked ){
        for(int i = 0 ; i < viewsIds.length; i++){
            if (checked){
                findViewById(viewsIds[i]).setVisibility(View.VISIBLE);
            }
            else{
                findViewById(viewsIds[i]).setVisibility(View.GONE);
            }
        }
    }

    public void updateViews(){
        recreate();
    }
}

