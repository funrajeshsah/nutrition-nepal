package com.nepget.rhino.utility;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by rajesh on 26/6/16.
 */
public class Data implements Serializable{

    //start of Nutirtion
    private Boolean muac_less_then_115 = false;
    private Boolean muac_more_then_115_and_less_then_125 = false;
    private Boolean bilateral_pitting_edema_present = false;
    private Boolean bilateral_pitting_edema_present_1plus_to_2plus = false;
    private Boolean bilateral_pitting_edema_present_3plus = false;
    private Boolean wt_for_height_less_then_minus_3sd = false;
    private Boolean wt_for_height_minus_2sd_to_minus_3sd = false;

    private Boolean not_eat_rutf = false;
    private Boolean demonstrate_appitie = false;

    private Boolean intractable_vomiting = false;
    private Boolean fever = false;
    private Boolean hypothermia = false;
    private Boolean lower_respiratory_tract_infection = false;
    private Boolean severe_anaemia = false;
    private Boolean not_alert = false;
    private Boolean jaundice_complication = false;
    private Boolean superficial_infection = false;
    private Boolean sever_dehydration = false;
    private Boolean eye_infection_and_other_eye_problem = false;

    private Boolean muac_less_then_21_cm = false;
    private Boolean muac_more_then_21cm_and_less_then_23cm = false;






    public Boolean getMuac_less_then_115() {
        return muac_less_then_115;
    }

    public void setMuac_less_then_115(Boolean muac_less_then_115) {
        this.muac_less_then_115 = muac_less_then_115;
    }

    public Boolean getMuac_more_then_115_and_less_then_125() {
        return muac_more_then_115_and_less_then_125;
    }

    public void setMuac_more_then_115_and_less_then_125(Boolean muac_more_then_115_and_less_then_125) {
        this.muac_more_then_115_and_less_then_125 = muac_more_then_115_and_less_then_125;
    }

    public Boolean getBilateral_pitting_edema_present() {
        return bilateral_pitting_edema_present;
    }

    public void setBilateral_pitting_edema_present(Boolean bilateral_pitting_edema_present) {
        this.bilateral_pitting_edema_present = bilateral_pitting_edema_present;
    }

    public Boolean getBilateral_pitting_edema_present_1plus_to_2plus() {
        return bilateral_pitting_edema_present_1plus_to_2plus;
    }

    public void setBilateral_pitting_edema_present_1plus_to_2plus(Boolean bilateral_pitting_edema_present_1plus_to_2plus) {
        this.bilateral_pitting_edema_present_1plus_to_2plus = bilateral_pitting_edema_present_1plus_to_2plus;
    }

    public Boolean getBilateral_pitting_edema_present_3plus() {
        return bilateral_pitting_edema_present_3plus;
    }

    public void setBilateral_pitting_edema_present_3plus(Boolean bilateral_pitting_edema_present_3plus) {
        this.bilateral_pitting_edema_present_3plus = bilateral_pitting_edema_present_3plus;
    }

    public Boolean getWt_for_height_less_then_minus_3sd() {
        return wt_for_height_less_then_minus_3sd;
    }

    public void setWt_for_height_less_then_minus_3sd(Boolean wt_for_height_less_then_minus_3sd) {
        this.wt_for_height_less_then_minus_3sd = wt_for_height_less_then_minus_3sd;
    }

    public Boolean getWt_for_height_minus_2sd_to_minus_3sd() {
        return wt_for_height_minus_2sd_to_minus_3sd;
    }

    public void setWt_for_height_minus_2sd_to_minus_3sd(Boolean wt_for_height_minus_2sd_to_minus_3sd) {
        this.wt_for_height_minus_2sd_to_minus_3sd = wt_for_height_minus_2sd_to_minus_3sd;
    }

    public Boolean getNot_eat_rutf() {
        return not_eat_rutf;
    }

    public void setNot_eat_rutf(Boolean not_eat_rutf) {
        this.not_eat_rutf = not_eat_rutf;
    }

    public Boolean getDemonstrate_appitie() {
        return demonstrate_appitie;
    }

    public void setDemonstrate_appitie(Boolean demonstrate_appitie) {
        this.demonstrate_appitie = demonstrate_appitie;
    }

    public Boolean getIntractable_vomiting() {
        return intractable_vomiting;
    }

    public void setIntractable_vomiting(Boolean intractable_vomiting) {
        this.intractable_vomiting = intractable_vomiting;
    }

    public Boolean getFever() {
        return fever;
    }

    public void setFever(Boolean fever) {
        this.fever = fever;
    }

    public Boolean getHypothermia() {
        return hypothermia;
    }

    public void setHypothermia(Boolean hypothermia) {
        this.hypothermia = hypothermia;
    }

    public Boolean getLower_respiratory_tract_infection() {
        return lower_respiratory_tract_infection;
    }

    public void setLower_respiratory_tract_infection(Boolean lower_respiratory_tract_infection) {
        this.lower_respiratory_tract_infection = lower_respiratory_tract_infection;
    }

    public Boolean getSevere_anaemia() {
        return severe_anaemia;
    }

    public void setSevere_anaemia(Boolean severe_anaemia) {
        this.severe_anaemia = severe_anaemia;
    }

    public Boolean getNot_alert() {
        return not_alert;
    }

    public void setNot_alert(Boolean not_alert) {
        this.not_alert = not_alert;
    }

    public Boolean getJaundice_complication() {
        return jaundice_complication;
    }

    public void setJaundice_complication(Boolean jaundice_complication) {
        this.jaundice_complication = jaundice_complication;
    }

    public Boolean getSuperficial_infection() {
        return superficial_infection;
    }

    public void setSuperficial_infection(Boolean superficial_infection) {
        this.superficial_infection = superficial_infection;
    }

    public Boolean getSever_dehydration() {
        return sever_dehydration;
    }

    public void setSever_dehydration(Boolean sever_dehydration) {
        this.sever_dehydration = sever_dehydration;
    }

    public Boolean getEye_infection_and_other_eye_problem() {
        return eye_infection_and_other_eye_problem;
    }

    public void setEye_infection_and_other_eye_problem(Boolean eye_infection_and_other_eye_problem) {
        this.eye_infection_and_other_eye_problem = eye_infection_and_other_eye_problem;
    }

    public Boolean getMuac_less_then_21_cm() {
        return muac_less_then_21_cm;
    }

    public void setMuac_less_then_21_cm(Boolean muac_less_then_21_cm) {
        this.muac_less_then_21_cm = muac_less_then_21_cm;
    }

    public Boolean getMuac_more_then_21cm_and_less_then_23cm() {
        return muac_more_then_21cm_and_less_then_23cm;
    }

    public void setMuac_more_then_21cm_and_less_then_23cm(Boolean muac_more_then_21cm_and_less_then_23cm) {
        this.muac_more_then_21cm_and_less_then_23cm = muac_more_then_21cm_and_less_then_23cm;
    }

    //end of nutirtion


    @Override
    public String toString() {
        return "Data{" +
                "muac_less_then_115=" + muac_less_then_115 +
                ", muac_more_then_115_and_less_then_125=" + muac_more_then_115_and_less_then_125 +
                ", bilateral_pitting_edema_present=" + bilateral_pitting_edema_present +
                ", bilateral_pitting_edema_present_1plus_to_2plus=" + bilateral_pitting_edema_present_1plus_to_2plus +
                ", bilateral_pitting_edema_present_3plus=" + bilateral_pitting_edema_present_3plus +
                ", wt_for_height_less_then_minus_3sd=" + wt_for_height_less_then_minus_3sd +
                ", wt_for_height_minus_2sd_to_minus_3sd=" + wt_for_height_minus_2sd_to_minus_3sd +
                ", not_eat_rutf=" + not_eat_rutf +
                ", demonstrate_appitie=" + demonstrate_appitie +
                ", intractable_vomiting=" + intractable_vomiting +
                ", fever=" + fever +
                ", hypothermia=" + hypothermia +
                ", lower_respiratory_tract_infection=" + lower_respiratory_tract_infection +
                ", severe_anaemia=" + severe_anaemia +
                ", not_alert=" + not_alert +
                ", jaundice_complication=" + jaundice_complication +
                ", superficial_infection=" + superficial_infection +
                ", sever_dehydration=" + sever_dehydration +
                ", eye_infection_and_other_eye_problem=" + eye_infection_and_other_eye_problem +
                ", muac_less_then_21_cm=" + muac_less_then_21_cm +
                ", muac_more_then_21cm_and_less_then_23cm=" + muac_more_then_21cm_and_less_then_23cm +
                '}';
    }

    public Data(){

    }
}
