package com.nepget.rhino.utility;

/**
 * Created by rajesh on 28/7/16.
 */
public class SelectedCheckboxData {
    String name;
    Integer string_value;

    public SelectedCheckboxData(String name, Integer string_value) {
        this.name = name;
        this.string_value = string_value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getString_value() {
        return string_value;
    }

    public void setString_value(Integer string_value) {
        this.string_value = string_value;
    }


}
