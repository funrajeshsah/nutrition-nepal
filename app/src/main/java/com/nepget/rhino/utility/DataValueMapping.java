package com.nepget.rhino.utility;
import com.nepget.rhino.R;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by rajesh on 27/7/16.
 */
public class DataValueMapping {

    private static final Map<String, Integer> stringNameValueMap;

    static {
        stringNameValueMap = new HashMap<String, Integer>();
        stringNameValueMap.put("drink_breastfeed", R.string.drink_breastfeed);
        stringNameValueMap.put("vomit_everything", R.string.vomit_everything);
        stringNameValueMap.put("convulsions", R.string.convulsions);
        stringNameValueMap.put("lethargic_or_unconscious", R.string.lethargic_or_unconscious);
        stringNameValueMap.put("convulsing_now", R.string.convulsing_now);

        stringNameValueMap.put("coughOrDifficultBreathing", R.string.coughOrDifficultBreathing);
        stringNameValueMap.put("chestIndrawing", R.string.chestIndrawing);
        stringNameValueMap.put("stridor", R.string.stridor);
        stringNameValueMap.put("fastBreathing", R.string.fastBreathing);
        stringNameValueMap.put("haveDiarrhoea", R.string.haveDiarrhoea);
        stringNameValueMap.put("diarrhoea_14_days_or_more", R.string.diarrhoea_14_days_or_more);
        stringNameValueMap.put("lethargicOrUnconscious", R.string.lethargicOrUnconscious);
        stringNameValueMap.put("restlessAndIrritable", R.string.restlessAndIrritable);
        stringNameValueMap.put("sunkenEyes", R.string.sunkenEyes);
        stringNameValueMap.put("NotAbleToDrinkOrDrinkingPoorly", R.string.NotAbleToDrinkOrDrinkingPoorly);
        stringNameValueMap.put("drinkingEagerlyThirsty", R.string.drinkingEagerlyThirsty);
        stringNameValueMap.put("SkinPinchGoesBackVerySlowly", R.string.SkinPinchGoesBackVerySlowly);
        stringNameValueMap.put("SkinPinchGoesBackSlowly", R.string.SkinPinchGoesBackSlowly);
        stringNameValueMap.put("bloodInStool", R.string.bloodInStool);

        stringNameValueMap.put("has_fever", R.string.has_fever);
        stringNameValueMap.put("malaria_risk", R.string.malaria_risk);
        stringNameValueMap.put("measles_within_the_last_3_months", R.string.measles_within_the_last_3_months);
        stringNameValueMap.put("stiff_neck", R.string.stiff_neck);
        stringNameValueMap.put("microscopic_rdt_positive", R.string.microscopic_rdt_positive);
        stringNameValueMap.put("fever_other_region", R.string.fever_other_region);
        stringNameValueMap.put("no_malaria_risk", R.string.no_malaria_risk);
        stringNameValueMap.put("falciparum_negatve", R.string.falciparum_negatve);
        stringNameValueMap.put("mesales_present", R.string.mesales_present);
        stringNameValueMap.put("runny_nose", R.string.runny_nose);


        stringNameValueMap.put("mouth_ulcers", R.string.mouth_ulcers);
        stringNameValueMap.put("deep_and_extensive", R.string.deep_and_extensive);
        stringNameValueMap.put("pus_draining_from_the_eye", R.string.pus_draining_from_the_eye);
        stringNameValueMap.put("clouding_of_the_cornea", R.string.clouding_of_the_cornea);


        stringNameValueMap.put("is_there_ear_pain", R.string.mouth_ulcers);
        stringNameValueMap.put("pus_draining", R.string.pus_draining);
        stringNameValueMap.put("tender_swelling_behind_the_ear", R.string.tender_swelling_behind_the_ear);
        stringNameValueMap.put("discharge_more_than_14_days", R.string.discharge_more_than_14_days);
        stringNameValueMap.put("discharge_less_than_14_days", R.string.discharge_less_than_14_days);
        stringNameValueMap.put("is_there_ear_problem", R.string.is_there_ear_problem);


        stringNameValueMap.put("oedema_of_both_feet", R.string.oedema_of_both_feet);
        stringNameValueMap.put("wfh_less_than_3_z_scores", R.string.wfh_less_than_3_z_scores);
        stringNameValueMap.put("wfh_between_2_3_z_scores", R.string.wfh_between_2_3_z_scores);
        stringNameValueMap.put("wfh_between_2_or_more_z_scores", R.string.wfh_between_2_or_more_z_scores);
        stringNameValueMap.put("MUAC_less_than_115_mm", R.string.MUAC_less_than_115_mm);
        stringNameValueMap.put("MUAC_115_up_to_125_mm", R.string.MUAC_115_up_to_125_mm);
        stringNameValueMap.put("MUAC_125_mm_or_more", R.string.MUAC_125_mm_or_more);
        stringNameValueMap.put("Medical_complication_present", R.string.Medical_complication_present);
        stringNameValueMap.put("weight_correct_as_height", R.string.weight_correct_as_height);


        stringNameValueMap.put("severe_palmar_pallor ", R.string.severe_palmar_pallor);
        stringNameValueMap.put("some_pallor", R.string.some_pallor);
        stringNameValueMap.put("no_whitness_in_palm", R.string.no_whitness_in_palm);

    /* start of less then 2 month */
        stringNameValueMap.put("convulsions_two ", R.string.convulsions_two);
        stringNameValueMap.put("fastBreathing_two ", R.string.fastBreathing_two);
        stringNameValueMap.put("severe_chest_indrawing_two ", R.string.severe_chest_indrawing_two);
        stringNameValueMap.put("swollen_nose ", R.string.swollen_nose);
        stringNameValueMap.put("Not_feeding_well_two ", R.string.Not_feeding_well_two);
        stringNameValueMap.put("baby_palate_bulging_two ", R.string.baby_palate_bulging_two);
        stringNameValueMap.put("navel_reddish_skin_stretched_two ", R.string.navel_reddish_skin_stretched_two);
        stringNameValueMap.put("fever_two ", R.string.fever_two);
        stringNameValueMap.put("low_body_temperature_two ", R.string.low_body_temperature_two);
        stringNameValueMap.put("many_skin_vesciles_two ", R.string.many_skin_vesciles_two);
        stringNameValueMap.put("Umbilicus_red_pus_two ", R.string.Umbilicus_red_pus_two);
        stringNameValueMap.put("skin_pustules_two ", R.string.skin_pustules_two);
        stringNameValueMap.put("purulent_discharge_eye_two ", R.string.purulent_discharge_eye_two);
        stringNameValueMap.put("movement_only_two ", R.string.movement_only_two);
        stringNameValueMap.put("Yellow_palms_and_soles_two ", R.string.Yellow_palms_and_soles_two);
        stringNameValueMap.put("age_less_24_two ", R.string.age_less_24_two);
        stringNameValueMap.put("age_more_14_two ", R.string.age_more_14_two);
        stringNameValueMap.put("Palms_soles_not_yellow_two ", R.string.Palms_soles_not_yellow_two);
        stringNameValueMap.put("Jaundice_after_24_two ", R.string.Jaundice_after_24_two);
        stringNameValueMap.put("temperature_less_than_35_5_two ", R.string.temperature_less_than_35_5_two);
        stringNameValueMap.put("temperature_mild_two ", R.string.temperature_mild_two);


        stringNameValueMap.put("does_the_child_have_diarrhoea_two ", R.string.does_the_child_have_diarrhoea_two);
        stringNameValueMap.put("Slow_or_is_unconscious_two ", R.string.Slow_or_is_unconscious_two);
        stringNameValueMap.put("movement_no_two ", R.string.movement_no_two);
        stringNameValueMap.put("sunken_eyes_two ", R.string.sunken_eyes_two);
        stringNameValueMap.put("skin_pinch_very_slowly_two ", R.string.skin_pinch_very_slowly_two);
        stringNameValueMap.put("infant_age_less_than_month_two ", R.string.infant_age_less_than_month_two);
        stringNameValueMap.put("Restless_and_irritable_two ", R.string.Restless_and_irritable_two);
        stringNameValueMap.put("skin_pinch_slowly_two ", R.string.skin_pinch_slowly_two);
        stringNameValueMap.put("diarrhoea_more_14_two ", R.string.diarrhoea_more_14_two);
        stringNameValueMap.put("blood_stool_two ", R.string.blood_stool_two);

        //stringNameValueMap.put("feeding_problem_low_weight_two ",R.string.feeding_problem_low_weight_two);
        stringNameValueMap.put("Not_well_attached_breast_two ", R.string.Not_well_attached_breast_two);
        stringNameValueMap.put("Not_sucking_effectively_two ", R.string.Not_sucking_effectively_two);
        stringNameValueMap.put("lees_8_breastfeeds_2_two4 ", R.string.lees_8_breastfeeds_2_two4);
        stringNameValueMap.put("receives_foods_drinks_two ", R.string.receives_foods_drinks_two);
        stringNameValueMap.put("low_weight_age_two ", R.string.low_weight_age_two);
        stringNameValueMap.put("thrus_ulcer_patches_two ", R.string.thrus_ulcer_patches_two);
        stringNameValueMap.put("not_low_weight_two ", R.string.not_low_weight_two);
    }

    public static Map<String, Integer> getStringNameValueMap() {
        return stringNameValueMap;
    }
}