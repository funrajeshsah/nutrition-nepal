package com.nepget.rhino.utility;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by rajesh on 28/7/16.
 */
public class FieldNameConstantMap {

    private static final Map<String, Integer> fieldNameConstantMap;

    static {
        fieldNameConstantMap = new HashMap<String, Integer>();
        fieldNameConstantMap.put("drink_breastfeed", FieldNameConstant.drink_breastfeed);
        fieldNameConstantMap.put("vomit_everything", FieldNameConstant.vomit_everything);
        fieldNameConstantMap.put("convulsions", FieldNameConstant.convulsions);
        fieldNameConstantMap.put("lethargic_or_unconscious", FieldNameConstant.lethargic_or_unconscious);
        fieldNameConstantMap.put("convulsing_now", FieldNameConstant.convulsing_now);

        fieldNameConstantMap.put("coughOrDifficultBreathing", FieldNameConstant.coughOrDifficultBreathing);
        fieldNameConstantMap.put("chestIndrawing", FieldNameConstant.chestIndrawing);
        fieldNameConstantMap.put("stridor", FieldNameConstant.stridor);
        fieldNameConstantMap.put("fastBreathing", FieldNameConstant.fastBreathing);
        fieldNameConstantMap.put("haveDiarrhoea", FieldNameConstant.haveDiarrhoea);
        fieldNameConstantMap.put("diarrhoea_14_days_or_more", FieldNameConstant.diarrhoea_14_days_or_more);
        fieldNameConstantMap.put("lethargicOrUnconscious", FieldNameConstant.lethargicOrUnconscious);
        fieldNameConstantMap.put("restlessAndIrritable", FieldNameConstant.restlessAndIrritable);
        fieldNameConstantMap.put("sunkenEyes", FieldNameConstant.sunkenEyes);
        fieldNameConstantMap.put("NotAbleToDrinkOrDrinkingPoorly", FieldNameConstant.NotAbleToDrinkOrDrinkingPoorly);
        fieldNameConstantMap.put("drinkingEagerlyThirsty", FieldNameConstant.drinkingEagerlyThirsty);
        fieldNameConstantMap.put("SkinPinchGoesBackVerySlowly", FieldNameConstant.SkinPinchGoesBackVerySlowly);
        fieldNameConstantMap.put("SkinPinchGoesBackSlowly", FieldNameConstant.SkinPinchGoesBackSlowly);
        fieldNameConstantMap.put("bloodInStool", FieldNameConstant.bloodInStool);

        fieldNameConstantMap.put("has_fever", FieldNameConstant.has_fever);
        fieldNameConstantMap.put("malaria_risk", FieldNameConstant.malaria_risk);
        fieldNameConstantMap.put("measles_within_the_last_3_months", FieldNameConstant.measles_within_the_last_3_months);
        fieldNameConstantMap.put("stiff_neck", FieldNameConstant.stiff_neck);
        fieldNameConstantMap.put("microscopic_rdt_positive", FieldNameConstant.microscopic_rdt_positive);
        fieldNameConstantMap.put("fever_other_region", FieldNameConstant.fever_other_region);
        fieldNameConstantMap.put("no_malaria_risk", FieldNameConstant.no_malaria_risk);
        fieldNameConstantMap.put("falciparum_negatve", FieldNameConstant.falciparum_negatve);
        fieldNameConstantMap.put("mesales_present", FieldNameConstant.mesales_present);
        fieldNameConstantMap.put("runny_nose", FieldNameConstant.runny_nose);


        fieldNameConstantMap.put("mouth_ulcers", FieldNameConstant.mouth_ulcers);
        fieldNameConstantMap.put("deep_and_extensive", FieldNameConstant.deep_and_extensive);
        fieldNameConstantMap.put("pus_draining_from_the_eye", FieldNameConstant.pus_draining_from_the_eye);
        fieldNameConstantMap.put("clouding_of_the_cornea", FieldNameConstant.clouding_of_the_cornea);


        fieldNameConstantMap.put("is_there_ear_pain", FieldNameConstant.mouth_ulcers);
        fieldNameConstantMap.put("pus_draining", FieldNameConstant.pus_draining);
        fieldNameConstantMap.put("tender_swelling_behind_the_ear", FieldNameConstant.tender_swelling_behind_the_ear);
        fieldNameConstantMap.put("discharge_more_than_14_days", FieldNameConstant.discharge_more_than_14_days);
        fieldNameConstantMap.put("discharge_less_than_14_days", FieldNameConstant.discharge_less_than_14_days);
        fieldNameConstantMap.put("is_there_ear_problem", FieldNameConstant.is_there_ear_problem);


        fieldNameConstantMap.put("oedema_of_both_feet", FieldNameConstant.oedema_of_both_feet);
        fieldNameConstantMap.put("wfh_less_than_3_z_scores", FieldNameConstant.wfh_less_than_3_z_scores);
        fieldNameConstantMap.put("wfh_between_2_3_z_scores", FieldNameConstant.wfh_between_2_3_z_scores);
        fieldNameConstantMap.put("wfh_between_2_or_more_z_scores", FieldNameConstant.wfh_between_2_or_more_z_scores);
        fieldNameConstantMap.put("MUAC_less_than_115_mm", FieldNameConstant.MUAC_less_than_115_mm);
        fieldNameConstantMap.put("MUAC_115_up_to_125_mm", FieldNameConstant.MUAC_115_up_to_125_mm);
        fieldNameConstantMap.put("MUAC_125_mm_or_more", FieldNameConstant.MUAC_125_mm_or_more);
        fieldNameConstantMap.put("Medical_complication_present", FieldNameConstant.Medical_complication_present);
        fieldNameConstantMap.put("weight_correct_as_height", FieldNameConstant.weight_correct_as_height);


        fieldNameConstantMap.put("severe_palmar_pallor ", FieldNameConstant.severe_palmar_pallor);
        fieldNameConstantMap.put("some_pallor", FieldNameConstant.some_pallor);
        fieldNameConstantMap.put("no_whitness_in_palm", FieldNameConstant.no_whitness_in_palm);

    /* start of less then 2 month */
        fieldNameConstantMap.put("convulsions_two ", FieldNameConstant.convulsions_two);
        fieldNameConstantMap.put("fastBreathing_two ", FieldNameConstant.fastBreathing_two);
        fieldNameConstantMap.put("severe_chest_indrawing_two ", FieldNameConstant.severe_chest_indrawing_two);
        fieldNameConstantMap.put("swollen_nose ", FieldNameConstant.swollen_nose);
        fieldNameConstantMap.put("Not_feeding_well_two ", FieldNameConstant.Not_feeding_well_two);
        fieldNameConstantMap.put("baby_palate_bulging_two ", FieldNameConstant.baby_palate_bulging_two);
        fieldNameConstantMap.put("navel_reddish_skin_stretched_two ", FieldNameConstant.navel_reddish_skin_stretched_two);
        fieldNameConstantMap.put("fever_two ", FieldNameConstant.fever_two);
        fieldNameConstantMap.put("low_body_temperature_two ", FieldNameConstant.low_body_temperature_two);
        fieldNameConstantMap.put("many_skin_vesciles_two ", FieldNameConstant.many_skin_vesciles_two);
        fieldNameConstantMap.put("Umbilicus_red_pus_two ", FieldNameConstant.Umbilicus_red_pus_two);
        fieldNameConstantMap.put("skin_pustules_two ", FieldNameConstant.skin_pustules_two);
        fieldNameConstantMap.put("purulent_discharge_eye_two ", FieldNameConstant.purulent_discharge_eye_two);
        fieldNameConstantMap.put("movement_only_two ", FieldNameConstant.movement_only_two);
        fieldNameConstantMap.put("Yellow_palms_and_soles_two ", FieldNameConstant.Yellow_palms_and_soles_two);
        fieldNameConstantMap.put("age_less_24_two ", FieldNameConstant.age_less_24_two);
        fieldNameConstantMap.put("age_more_14_two ", FieldNameConstant.age_more_14_two);
        fieldNameConstantMap.put("Palms_soles_not_yellow_two ", FieldNameConstant.Palms_soles_not_yellow_two);
        fieldNameConstantMap.put("Jaundice_after_24_two ", FieldNameConstant.Jaundice_after_24_two);
        fieldNameConstantMap.put("temperature_less_than_35_5_two ", FieldNameConstant.temperature_less_than_35_5_two);
        fieldNameConstantMap.put("temperature_mild_two ", FieldNameConstant.temperature_mild_two);


        fieldNameConstantMap.put("does_the_child_have_diarrhoea_two ", FieldNameConstant.does_the_child_have_diarrhoea_two);
        fieldNameConstantMap.put("Slow_or_is_unconscious_two ", FieldNameConstant.Slow_or_is_unconscious_two);
        fieldNameConstantMap.put("movement_no_two ", FieldNameConstant.movement_no_two);
        fieldNameConstantMap.put("sunken_eyes_two ", FieldNameConstant.sunken_eyes_two);
        fieldNameConstantMap.put("skin_pinch_very_slowly_two ", FieldNameConstant.skin_pinch_very_slowly_two);
        fieldNameConstantMap.put("infant_age_less_than_month_two ", FieldNameConstant.infant_age_less_than_month_two);
        fieldNameConstantMap.put("Restless_and_irritable_two ", FieldNameConstant.Restless_and_irritable_two);
        fieldNameConstantMap.put("skin_pinch_slowly_two ", FieldNameConstant.skin_pinch_slowly_two);
        fieldNameConstantMap.put("diarrhoea_more_14_two ", FieldNameConstant.diarrhoea_more_14_two);
        fieldNameConstantMap.put("blood_stool_two ", FieldNameConstant.blood_stool_two);

        //fieldNameConstantMap.put("feeding_problem_low_weight_two ",FieldNameConstant.feeding_problem_low_weight_two);
        fieldNameConstantMap.put("Not_well_attached_breast_two ", FieldNameConstant.Not_well_attached_breast_two);
        fieldNameConstantMap.put("Not_sucking_effectively_two ", FieldNameConstant.Not_sucking_effectively_two);
        fieldNameConstantMap.put("lees_8_breastfeeds_2_two4 ", FieldNameConstant.lees_8_breastfeeds_2_two4);
        fieldNameConstantMap.put("receives_foods_drinks_two ", FieldNameConstant.receives_foods_drinks_two);
        fieldNameConstantMap.put("low_weight_age_two ", FieldNameConstant.low_weight_age_two);
        fieldNameConstantMap.put("thrus_ulcer_patches_two ", FieldNameConstant.thrus_ulcer_patches_two);
        fieldNameConstantMap.put("not_low_weight_two ", FieldNameConstant.not_low_weight_two);

        //classify
        fieldNameConstantMap.put("classfy_very_sever_disease", FieldNameConstant.classfy_very_sever_disease);
        fieldNameConstantMap.put("classfy_SEVERE_PNEUMONIA_OR_VERY_SEVERE_DISEASE", FieldNameConstant.classfy_SEVERE_PNEUMONIA_OR_VERY_SEVERE_DISEASE);
        fieldNameConstantMap.put("classfy_PNEUMONIA", FieldNameConstant.classfy_PNEUMONIA);
        fieldNameConstantMap.put("classfy_cough_or_cold", FieldNameConstant.classfy_cough_or_cold);

        fieldNameConstantMap.put("classify_SEVERE_PERSISTENT_DIARRHOEA", FieldNameConstant.classify_SEVERE_PERSISTENT_DIARRHOEA);
        fieldNameConstantMap.put("classify_PERSISTENT_DIARRHOEA", FieldNameConstant.classify_PERSISTENT_DIARRHOEA);
        fieldNameConstantMap.put("classify_DYSENTERY", FieldNameConstant.classify_DYSENTERY);
        fieldNameConstantMap.put("classfy_sever_dehydration", FieldNameConstant.classfy_sever_dehydration);
        fieldNameConstantMap.put("classfy_some_dehydration", FieldNameConstant.classfy_some_dehydration);
        fieldNameConstantMap.put("classfy_no_dehydration", FieldNameConstant.classfy_no_dehydration);

        fieldNameConstantMap.put("classify_VERY_SEVERE_FEBRILE_DISEASE", FieldNameConstant.classify_VERY_SEVERE_FEBRILE_DISEASE);
        fieldNameConstantMap.put("classify_MALARIA_Falciparum", FieldNameConstant.classify_MALARIA_Falciparum);
        fieldNameConstantMap.put("classify_MALARIA_without_Falciparum", FieldNameConstant.classify_MALARIA_without_Falciparum);
        fieldNameConstantMap.put("classify_FEVER_no_malaria", FieldNameConstant.classify_FEVER_no_malaria);
        fieldNameConstantMap.put("classify_severe_complicated_measle", FieldNameConstant.classify_severe_complicated_measle);
        fieldNameConstantMap.put("classify_MEASLES_WITH_EYE_OR_MOUTH_COMPLICATIONS", FieldNameConstant.classify_MEASLES_WITH_EYE_OR_MOUTH_COMPLICATIONS);
        fieldNameConstantMap.put("classify_MEASLES", FieldNameConstant.classify_MEASLES);
        fieldNameConstantMap.put("classify_no_measelsVERY_SEVERE_FEBRILE_DISEASE", FieldNameConstant.classify_no_measelsVERY_SEVERE_FEBRILE_DISEASE);
        fieldNameConstantMap.put("classify_fever", FieldNameConstant.classify_fever);

        fieldNameConstantMap.put("classify_MASTOIDITIS", FieldNameConstant.classify_MASTOIDITIS);
        fieldNameConstantMap.put("classify_CHRONIC_EAR_INFECTION", FieldNameConstant.classify_CHRONIC_EAR_INFECTION);
        fieldNameConstantMap.put("classify_ACUTE_EAR_INFECTION", FieldNameConstant.classify_ACUTE_EAR_INFECTION);
        fieldNameConstantMap.put("classify_NO_EAR_INFECTION", FieldNameConstant.classify_NO_EAR_INFECTION);


        fieldNameConstantMap.put("classify_UNCOMPLICATED_SEVERE_ACUTE_MALNUTRITION", FieldNameConstant.classify_UNCOMPLICATED_SEVERE_ACUTE_MALNUTRITION);
        fieldNameConstantMap.put("classify_MODERATE_ACUTE_MALNUTRITION", FieldNameConstant.classify_MODERATE_ACUTE_MALNUTRITION);
        fieldNameConstantMap.put("classify_NO_ACUTE_MALNUTRITION", FieldNameConstant.classify_NO_ACUTE_MALNUTRITION);

        fieldNameConstantMap.put("classify_SEVERE_ANAEMIA", FieldNameConstant.classify_SEVERE_ANAEMIA);
        fieldNameConstantMap.put("classify_some_ANAEMIA", FieldNameConstant.classify_some_ANAEMIA);
        fieldNameConstantMap.put("classify_no_ANAEMIA", FieldNameConstant.classify_no_ANAEMIA);


    }

    public static Map<String, Integer> getFieldNameConstantMap() {
        return fieldNameConstantMap;
    }
}

