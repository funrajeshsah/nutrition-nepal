package com.nepget.rhino.utility;

/**
 * Created by rajesh on 30/1/17.
 */
public class User {
    public String username;
    public String email;
    public String createdAt;

    public User() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public User(String username, String email, String createdAt) {
        this.username = username;
        this.email = email;
        this.createdAt = createdAt;

    }
}
