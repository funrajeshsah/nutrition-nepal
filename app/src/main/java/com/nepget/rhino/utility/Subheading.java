package com.nepget.rhino.utility;

/**
 * Created by rajesh on 27/1/17.
 */
public class Subheading {

    private String html;
    private String text;

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "Subheading{" +
                "html='" + html + '\'' +
                ", text='" + text + '\'' +
                '}';
    }
}