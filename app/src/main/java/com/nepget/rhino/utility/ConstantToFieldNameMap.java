package com.nepget.rhino.utility;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by rajesh on 28/7/16.
 */
public class ConstantToFieldNameMap {

    private static final Map<Integer, String> constantFieldNameMap;

    static {
        constantFieldNameMap = new HashMap<Integer, String>();
        constantFieldNameMap.put(FieldNameConstant.drink_breastfeed, "drink_breastfeed");
        constantFieldNameMap.put(FieldNameConstant.vomit_everything, "vomit_everything");
        constantFieldNameMap.put(FieldNameConstant.convulsions, "convulsions");
        constantFieldNameMap.put(FieldNameConstant.lethargic_or_unconscious, "lethargic_or_unconscious");
        constantFieldNameMap.put(FieldNameConstant.convulsing_now, "convulsing_now");

        constantFieldNameMap.put(FieldNameConstant.coughOrDifficultBreathing, "coughOrDifficultBreathing");
        constantFieldNameMap.put(FieldNameConstant.chestIndrawing, "chestIndrawing");
        constantFieldNameMap.put(FieldNameConstant.stridor, "stridor");
        constantFieldNameMap.put(FieldNameConstant.fastBreathing, "fastBreathing");
        constantFieldNameMap.put(FieldNameConstant.haveDiarrhoea, "haveDiarrhoea");
        constantFieldNameMap.put(FieldNameConstant.diarrhoea_14_days_or_more, "diarrhoea_14_days_or_more");
        constantFieldNameMap.put(FieldNameConstant.lethargicOrUnconscious, "lethargicOrUnconscious");
        constantFieldNameMap.put(FieldNameConstant.restlessAndIrritable, "restlessAndIrritable");
        constantFieldNameMap.put(FieldNameConstant.sunkenEyes, "sunkenEyes");
        constantFieldNameMap.put(FieldNameConstant.NotAbleToDrinkOrDrinkingPoorly, "NotAbleToDrinkOrDrinkingPoorly");
        constantFieldNameMap.put(FieldNameConstant.drinkingEagerlyThirsty, "drinkingEagerlyThirsty");
        constantFieldNameMap.put(FieldNameConstant.SkinPinchGoesBackVerySlowly, "SkinPinchGoesBackVerySlowly");
        constantFieldNameMap.put(FieldNameConstant.SkinPinchGoesBackSlowly, "SkinPinchGoesBackSlowly");
        constantFieldNameMap.put(FieldNameConstant.bloodInStool, "bloodInStool");

        constantFieldNameMap.put(FieldNameConstant.has_fever, "has_fever");
        constantFieldNameMap.put(FieldNameConstant.malaria_risk, "malaria_risk");
        constantFieldNameMap.put(FieldNameConstant.measles_within_the_last_3_months, "measles_within_the_last_3_months");
        constantFieldNameMap.put(FieldNameConstant.stiff_neck, "stiff_neck");
        constantFieldNameMap.put(FieldNameConstant.microscopic_rdt_positive, "microscopic_rdt_positive");
        constantFieldNameMap.put(FieldNameConstant.fever_other_region, "fever_other_region");
        constantFieldNameMap.put(FieldNameConstant.no_malaria_risk, "no_malaria_risk");
        constantFieldNameMap.put(FieldNameConstant.falciparum_negatve, "falciparum_negatve");
        constantFieldNameMap.put(FieldNameConstant.mesales_present, "mesales_present");
        constantFieldNameMap.put(FieldNameConstant.runny_nose, "runny_nose");


        constantFieldNameMap.put(FieldNameConstant.mouth_ulcers, "mouth_ulcers");
        constantFieldNameMap.put(FieldNameConstant.deep_and_extensive, "deep_and_extensive");
        constantFieldNameMap.put(FieldNameConstant.pus_draining_from_the_eye, "pus_draining_from_the_eye");
        constantFieldNameMap.put(FieldNameConstant.clouding_of_the_cornea, "clouding_of_the_cornea");


        constantFieldNameMap.put(FieldNameConstant.is_there_ear_pain, "mouth_ulcers");
        constantFieldNameMap.put(FieldNameConstant.pus_draining, "pus_draining");
        constantFieldNameMap.put(FieldNameConstant.tender_swelling_behind_the_ear, "tender_swelling_behind_the_ear");
        constantFieldNameMap.put(FieldNameConstant.discharge_more_than_14_days, "discharge_more_than_14_days");
        constantFieldNameMap.put(FieldNameConstant.discharge_less_than_14_days, "discharge_less_than_14_days");
        constantFieldNameMap.put(FieldNameConstant.is_there_ear_problem, "is_there_ear_problem");


        constantFieldNameMap.put(FieldNameConstant.oedema_of_both_feet, "oedema_of_both_feet");
        constantFieldNameMap.put(FieldNameConstant.wfh_less_than_3_z_scores, "wfh_less_than_3_z_scores");
        constantFieldNameMap.put(FieldNameConstant.wfh_between_2_3_z_scores, "wfh_between_2_3_z_scores");
        constantFieldNameMap.put(FieldNameConstant.wfh_between_2_or_more_z_scores, "wfh_between_2_or_more_z_scores");
        constantFieldNameMap.put(FieldNameConstant.MUAC_less_than_115_mm, "MUAC_less_than_115_mm");
        constantFieldNameMap.put(FieldNameConstant.MUAC_115_up_to_125_mm, "MUAC_115_up_to_125_mm");
        constantFieldNameMap.put(FieldNameConstant.MUAC_125_mm_or_more, "MUAC_125_mm_or_more");
        constantFieldNameMap.put(FieldNameConstant.Medical_complication_present, "Medical_complication_present");
        constantFieldNameMap.put(FieldNameConstant.weight_correct_as_height, "weight_correct_as_height");


        constantFieldNameMap.put(FieldNameConstant.severe_palmar_pallor , "severe_palmar_pallor");
        constantFieldNameMap.put(FieldNameConstant.some_pallor, "some_pallor");
        constantFieldNameMap.put(FieldNameConstant.no_whitness_in_palm, "no_whitness_in_palm");

    /* start of less then 2 month */
        constantFieldNameMap.put(FieldNameConstant.convulsions_two , "convulsions_two");
        constantFieldNameMap.put(FieldNameConstant.fastBreathing_two , "fastBreathing_two");
        constantFieldNameMap.put(FieldNameConstant.severe_chest_indrawing_two , "severe_chest_indrawing_two");
        constantFieldNameMap.put(FieldNameConstant.swollen_nose , "swollen_nose");
        constantFieldNameMap.put(FieldNameConstant.Not_feeding_well_two , "Not_feeding_well_two");
        constantFieldNameMap.put(FieldNameConstant.baby_palate_bulging_two , "baby_palate_bulging_two");
        constantFieldNameMap.put(FieldNameConstant.navel_reddish_skin_stretched_two , "navel_reddish_skin_stretched_two");
        constantFieldNameMap.put(FieldNameConstant.fever_two , "fever_two");
        constantFieldNameMap.put(FieldNameConstant.low_body_temperature_two , "low_body_temperature_two");
        constantFieldNameMap.put(FieldNameConstant.many_skin_vesciles_two , "many_skin_vesciles_two");
        constantFieldNameMap.put(FieldNameConstant.Umbilicus_red_pus_two , "Umbilicus_red_pus_two");
        constantFieldNameMap.put(FieldNameConstant.skin_pustules_two , "skin_pustules_two");
        constantFieldNameMap.put(FieldNameConstant.purulent_discharge_eye_two , "purulent_discharge_eye_two");
        constantFieldNameMap.put(FieldNameConstant.movement_only_two , "movement_only_two");
        constantFieldNameMap.put(FieldNameConstant.Yellow_palms_and_soles_two , "Yellow_palms_and_soles_two");
        constantFieldNameMap.put(FieldNameConstant.age_less_24_two , "age_less_24_two");
        constantFieldNameMap.put(FieldNameConstant.age_more_14_two , "age_more_14_two");
        constantFieldNameMap.put(FieldNameConstant.Palms_soles_not_yellow_two , "Palms_soles_not_yellow_two");
        constantFieldNameMap.put(FieldNameConstant.Jaundice_after_24_two , "Jaundice_after_24_two");
        constantFieldNameMap.put(FieldNameConstant.temperature_less_than_35_5_two , "temperature_less_than_35_5_two");
        constantFieldNameMap.put(FieldNameConstant.temperature_mild_two , "temperature_mild_two");


        constantFieldNameMap.put(FieldNameConstant.does_the_child_have_diarrhoea_two , "does_the_child_have_diarrhoea_two");
        constantFieldNameMap.put(FieldNameConstant.Slow_or_is_unconscious_two , "Slow_or_is_unconscious_two");
        constantFieldNameMap.put(FieldNameConstant.movement_no_two , "movement_no_two");
        constantFieldNameMap.put(FieldNameConstant.sunken_eyes_two , "sunken_eyes_two");
        constantFieldNameMap.put(FieldNameConstant.skin_pinch_very_slowly_two , "skin_pinch_very_slowly_two");
        constantFieldNameMap.put(FieldNameConstant.infant_age_less_than_month_two , "infant_age_less_than_month_two");
        constantFieldNameMap.put(FieldNameConstant.Restless_and_irritable_two , "Restless_and_irritable_two");
        constantFieldNameMap.put(FieldNameConstant.skin_pinch_slowly_two , "skin_pinch_slowly_two");
        constantFieldNameMap.put(FieldNameConstant.diarrhoea_more_14_two , "diarrhoea_more_14_two");
        constantFieldNameMap.put(FieldNameConstant.blood_stool_two , "blood_stool_two");

        //constantFieldNameMap.put(FieldNameConstant.feeding_problem_low_weight_two ",FieldNameConstant.feeding_problem_low_weight_two");
        constantFieldNameMap.put(FieldNameConstant.Not_well_attached_breast_two , "Not_well_attached_breast_two");
        constantFieldNameMap.put(FieldNameConstant.Not_sucking_effectively_two , "Not_sucking_effectively_two");
        constantFieldNameMap.put(FieldNameConstant.lees_8_breastfeeds_2_two4 , "lees_8_breastfeeds_2_two4");
        constantFieldNameMap.put(FieldNameConstant.receives_foods_drinks_two , "receives_foods_drinks_two");
        constantFieldNameMap.put(FieldNameConstant.low_weight_age_two , "low_weight_age_two");
        constantFieldNameMap.put(FieldNameConstant.thrus_ulcer_patches_two , "thrus_ulcer_patches_two");
        constantFieldNameMap.put(FieldNameConstant.not_low_weight_two , "not_low_weight_two");
    }
    public static Map<Integer, String> getConstantFieldNameMap() {
        return constantFieldNameMap;
    }
}

