package com.nepget.rhino.utility;

/**
 * Created by rajesh on 28/7/16.
 */
public class FieldNameConstant {

    public static final Integer  drink_breastfeed = 0;
    public static final Integer  vomit_everything = 1;
    public static final Integer  convulsions = 2;
    public static final Integer  lethargic_or_unconscious = 3;
    public static final Integer   convulsing_now = 4;


    public static final Integer   coughOrDifficultBreathing = 5;
    public static final Integer   chestIndrawing = 6;
    public static final Integer   stridor = 7;
    public static final Integer   fastBreathing = 8;
    public static final Integer   haveDiarrhoea = 9;
    public static final Integer   diarrhoea_14_days_or_more = 10;
    public static final Integer   lethargicOrUnconscious = 11;
    public static final Integer   restlessAndIrritable = 12;
    public static final Integer   sunkenEyes = 13;
    public static final Integer   NotAbleToDrinkOrDrinkingPoorly = 14;
    public static final Integer   drinkingEagerlyThirsty = 15;
    public static final Integer   SkinPinchGoesBackVerySlowly = 16;
    public static final Integer   SkinPinchGoesBackSlowly = 17;
    public static final Integer  bloodInStool = 18;

    public static final Integer   has_fever = 19;
    public static final Integer   malaria_risk = 20;
    public static final Integer   measles_within_the_last_3_months = 21;
    public static final Integer   stiff_neck = 22;
    public static final Integer   microscopic_rdt_positive = 23;
    public static final Integer   fever_other_region = 24;
    public static final Integer   no_malaria_risk = 25;
    public static final Integer   falciparum_negatve = 26;
    public static final Integer   mesales_present = 27;
    public static final Integer   runny_nose = 28;


    public static final Integer   mouth_ulcers = 29;
    public static final Integer   deep_and_extensive = 30;
    public static final Integer   pus_draining_from_the_eye = 31;
    public static final Integer   clouding_of_the_cornea = 32;




    public static final Integer   is_there_ear_pain = 33;
    public static final Integer   pus_draining = 34;
    public static final Integer   tender_swelling_behind_the_ear = 35;
    public static final Integer   discharge_more_than_14_days = 36;
    public static final Integer  discharge_less_than_14_days = 37;
    public static final Integer  is_there_ear_problem = 38;


    public static final Integer  oedema_of_both_feet = 39;
    public static final Integer  wfh_less_than_3_z_scores = 40;
    public static final Integer  wfh_between_2_3_z_scores = 41;
    public static final Integer  wfh_between_2_or_more_z_scores = 42;
    public static final Integer  MUAC_less_than_115_mm = 43;
    public static final Integer  MUAC_115_up_to_125_mm = 44;
    public static final Integer  MUAC_125_mm_or_more = 45;
    public static final Integer  Medical_complication_present = 46;
    public static final Integer  weight_correct_as_height = 47;


    public static final Integer  severe_palmar_pallor  = 48;
    public static final Integer  some_pallor = 49;
    public static final Integer  no_whitness_in_palm = 50;

    /* start of less then 2 month */
    public static final Integer  convulsions_two  = 51;
    public static final Integer  fastBreathing_two  = 52;
    public static final Integer  severe_chest_indrawing_two  = 53;
    public static final Integer  swollen_nose  = 54;
    public static final Integer  Not_feeding_well_two  = 55;
    public static final Integer  baby_palate_bulging_two  = 56;
    public static final Integer  navel_reddish_skin_stretched_two  = 57;
    public static final Integer  fever_two  = 58;
    public static final Integer  low_body_temperature_two  = 59;
    public static final Integer  many_skin_vesciles_two  = 60;
    public static final Integer  Umbilicus_red_pus_two  = 61;
    public static final Integer  skin_pustules_two  = 62;
    public static final Integer  purulent_discharge_eye_two  = 63;
    public static final Integer  movement_only_two  = 64;
    public static final Integer  Yellow_palms_and_soles_two  = 65;
    public static final Integer  age_less_24_two  = 66;
    public static final Integer  age_more_14_two  = 67;
    public static final Integer  Palms_soles_not_yellow_two  = 68;
    public static final Integer  Jaundice_after_24_two  = 69;
    public static final Integer  temperature_less_than_35_5_two  = 70;
    public static final Integer  temperature_mild_two  = 71;


    public static final Integer  does_the_child_have_diarrhoea_two  = 72;
    public static final Integer  Slow_or_is_unconscious_two  = 73;
    public static final Integer  movement_no_two  = 74;
    public static final Integer  sunken_eyes_two  = 75;
    public static final Integer  skin_pinch_very_slowly_two  = 76;
    public static final Integer  infant_age_less_than_month_two  = 77;
    public static final Integer  Restless_and_irritable_two  = 78;
    public static final Integer  skin_pinch_slowly_two  = 79;
    public static final Integer  diarrhoea_more_14_two  = 80;
    public static final Integer  blood_stool_two  = 81;

    public static final Integer  feeding_problem_low_weight_two  = 82;
    public static final Integer  Not_well_attached_breast_two  = 83;
    public static final Integer  Not_sucking_effectively_two  = 84;
    public static final Integer  lees_8_breastfeeds_2_two4  = 85;
    public static final Integer  receives_foods_drinks_two  = 86;
    public static final Integer  low_weight_age_two  = 87;
    public static final Integer  thrus_ulcer_patches_two  = 88;
    public static final Integer  not_low_weight_two  = 89;

    public static final Integer  classfy_very_sever_disease  = 90;
    public static final Integer  classfy_SEVERE_PNEUMONIA_OR_VERY_SEVERE_DISEASE  = 91;
    public static final Integer  classfy_PNEUMONIA  = 92;
    public static final Integer  classfy_cough_or_cold  = 93;

    public static final Integer  classify_SEVERE_PERSISTENT_DIARRHOEA  = 94;
    public static final Integer  classify_PERSISTENT_DIARRHOEA  = 94;
    public static final Integer  classify_DYSENTERY  = 95;
    public static final Integer  classfy_sever_dehydration  = 96;
    public static final Integer  classfy_some_dehydration  = 97;
    public static final Integer  classfy_no_dehydration  = 98;

    public static final Integer  classify_VERY_SEVERE_FEBRILE_DISEASE  = 99;
    public static final Integer  classify_MALARIA_Falciparum  = 100;
    public static final Integer  classify_MALARIA_without_Falciparum  = 101;
    public static final Integer  classify_FEVER_no_malaria  = 102;
    public static final Integer  classify_severe_complicated_measle  = 103;
    public static final Integer  classify_MEASLES_WITH_EYE_OR_MOUTH_COMPLICATIONS  = 104;
    public static final Integer  classify_MEASLES  = 105;
    public static final Integer  classify_no_measelsVERY_SEVERE_FEBRILE_DISEASE  = 106;
    public static final Integer  classify_fever  = 107;

    public static final Integer  classify_MASTOIDITIS  = 108;
    public static final Integer  classify_CHRONIC_EAR_INFECTION  = 109;
    public static final Integer  classify_ACUTE_EAR_INFECTION  = 110;
    public static final Integer  classify_NO_EAR_INFECTION  = 111;


    public static final Integer  classify_UNCOMPLICATED_SEVERE_ACUTE_MALNUTRITION  = 112;
    public static final Integer  classify_MODERATE_ACUTE_MALNUTRITION  = 113;
    public static final Integer  classify_NO_ACUTE_MALNUTRITION  = 114;

    public static final Integer  classify_SEVERE_ANAEMIA  = 115;
    public static final Integer  classify_some_ANAEMIA  = 116;
    public static final Integer  classify_no_ANAEMIA  = 117;

}
