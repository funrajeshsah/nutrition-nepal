package com.nepget.rhino.utility;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by rajesh on 27/1/17.
 */
public class Treatment {
    private List<Headings> headings = null;
    private Map<String,Subheading> subheadingMap = new HashMap<String,Subheading>();


    public List<Headings> getHeadings() {
        return headings;
    }

    public void setHeadings(List<Headings> headings) {
        this.headings = headings;
        for(Headings h:headings) {
            List<Subheading> subheadings = h.getSubheading();
            for(Subheading s:subheadings){
                subheadingMap.put(h.getText()+"__"+s.getText(),s);
            }
        }
    }

    public Map<String, Subheading> getSubheadingMap() {
        return subheadingMap;
    }

    @Override
    public String toString() {
        return "Treatment {" +
                "headings=" + headings +
                '}';
    }
}
