package com.nepget.rhino.utility;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rajesh on 27/7/16.
 */
public class DataSelectedKey {

    public static List<SelectedCheckboxData> getSelectedCheckbox(Data data){
        List<SelectedCheckboxData> output = new ArrayList<SelectedCheckboxData>(100);
        String temp = data.toString();
        String[] parts = temp.split(",");
        for(int i=0 ; i< parts.length; i++){
            System.out.println(parts[i]);
            String[] result = parts[i].split("=");
            System.out.println(result[0] + " : "+ result[1]);
            if(result[1].equalsIgnoreCase("true")){
                SelectedCheckboxData selectedData = new SelectedCheckboxData(result[0].trim(),DataValueMapping.getStringNameValueMap().get(result[0].trim()));
                output.add(selectedData);
            }
        }

        return output;
    }
}
