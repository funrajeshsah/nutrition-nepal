package com.nepget.rhino.utility;

/**
 * Created by rajesh on 28/7/16.
 */
public class Appointment {
    private String doctorUnqueKey ;
    private String signAndSymptoms ;
    private String classifiedResults;
    private boolean dataSynced;

    public Appointment(String doctorUnqueKey, String signAndSymptoms, String classifiedResults, boolean dataSynced) {
        this.doctorUnqueKey = doctorUnqueKey;
        this.signAndSymptoms = signAndSymptoms;
        this.classifiedResults = classifiedResults;
        this.dataSynced = dataSynced;
    }

    public Appointment() {
        this.doctorUnqueKey = "defaultDoctorUnqueKey";
        this.signAndSymptoms = "defaultSignAndSymptoms";
        this.classifiedResults = "defaultClassifiedResults";
        this.dataSynced = false;
    }

    public String getDoctorUnqueKey() {
        return doctorUnqueKey;
    }

    public void setDoctorUnqueKey(String doctorUnqueKey) {
        this.doctorUnqueKey = doctorUnqueKey;
    }

    public String getSignAndSymptoms() {
        return signAndSymptoms;
    }

    public void setSignAndSymptoms(String signAndSymptoms) {
        this.signAndSymptoms = signAndSymptoms;
    }

    public String getClassifiedResults() {
        return classifiedResults;
    }

    public void setClassifiedResults(String classifiedResults) {
        this.classifiedResults = classifiedResults;
    }

    public boolean isDataSynced() {
        return dataSynced;
    }

    public void setDataSynced(boolean dataSynced) {
        this.dataSynced = dataSynced;
    }
}
