package com.nepget.rhino.utility;

/**
 * Created by rajesh on 27/1/17.
 */
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Headings {

    private List<Subheading> subheading = null;
    private String text;


    public List<Subheading> getSubheading() {
        return subheading;
    }



    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "Headings{" +
                "subheading=" + subheading +
                ", text='" + text + '\'' +
                '}';
    }


}