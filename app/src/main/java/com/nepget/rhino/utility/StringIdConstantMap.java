package com.nepget.rhino.utility;

import com.nepget.rhino.R;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by rajesh on 28/7/16.
 */
public class StringIdConstantMap {

    public static Map<Integer, Integer> getStringIdConstantMap() {
        Map<Integer, Integer> stringIdConstantMap = new HashMap<Integer, Integer>();
        stringIdConstantMap.put(R.string.drink_breastfeed, FieldNameConstantMap.getFieldNameConstantMap().get("drink_breastfeed"));
        stringIdConstantMap.put(R.string.vomit_everything, FieldNameConstantMap.getFieldNameConstantMap().get("vomit_everything"));
        stringIdConstantMap.put(R.string.convulsions, FieldNameConstantMap.getFieldNameConstantMap().get("convulsions"));
        stringIdConstantMap.put(R.string.lethargic_or_unconscious, FieldNameConstantMap.getFieldNameConstantMap().get("lethargic_or_unconscious"));
        stringIdConstantMap.put(R.string.convulsing_now, FieldNameConstantMap.getFieldNameConstantMap().get("convulsing_now"));

        stringIdConstantMap.put(R.string.coughOrDifficultBreathing, FieldNameConstantMap.getFieldNameConstantMap().get("coughOrDifficultBreathing"));
        stringIdConstantMap.put(R.string.chestIndrawing, FieldNameConstantMap.getFieldNameConstantMap().get("chestIndrawing"));
        stringIdConstantMap.put(R.string.stridor, FieldNameConstantMap.getFieldNameConstantMap().get("stridor"));
        stringIdConstantMap.put(R.string.fastBreathing, FieldNameConstantMap.getFieldNameConstantMap().get("fastBreathing"));
        stringIdConstantMap.put(R.string.haveDiarrhoea, FieldNameConstantMap.getFieldNameConstantMap().get("haveDiarrhoea"));
        stringIdConstantMap.put(R.string.diarrhoea_14_days_or_more, FieldNameConstantMap.getFieldNameConstantMap().get("diarrhoea_14_days_or_more"));
        stringIdConstantMap.put(R.string.lethargicOrUnconscious, FieldNameConstantMap.getFieldNameConstantMap().get("lethargicOrUnconscious"));
        stringIdConstantMap.put(R.string.restlessAndIrritable, FieldNameConstantMap.getFieldNameConstantMap().get("restlessAndIrritable"));
        stringIdConstantMap.put(R.string.sunkenEyes, FieldNameConstantMap.getFieldNameConstantMap().get("sunkenEyes"));
        stringIdConstantMap.put(R.string.NotAbleToDrinkOrDrinkingPoorly, FieldNameConstantMap.getFieldNameConstantMap().get("NotAbleToDrinkOrDrinkingPoorly"));
        stringIdConstantMap.put(R.string.drinkingEagerlyThirsty, FieldNameConstantMap.getFieldNameConstantMap().get("drinkingEagerlyThirsty"));
        stringIdConstantMap.put(R.string.SkinPinchGoesBackVerySlowly, FieldNameConstantMap.getFieldNameConstantMap().get("SkinPinchGoesBackVerySlowly"));
        stringIdConstantMap.put(R.string.SkinPinchGoesBackSlowly, FieldNameConstantMap.getFieldNameConstantMap().get("SkinPinchGoesBackSlowly"));
        stringIdConstantMap.put(R.string.bloodInStool, FieldNameConstantMap.getFieldNameConstantMap().get("bloodInStool"));

        stringIdConstantMap.put(R.string.has_fever, FieldNameConstantMap.getFieldNameConstantMap().get("has_fever"));
        stringIdConstantMap.put(R.string.malaria_risk, FieldNameConstantMap.getFieldNameConstantMap().get("malaria_risk"));
        stringIdConstantMap.put(R.string.measles_within_the_last_3_months, FieldNameConstantMap.getFieldNameConstantMap().get("measles_within_the_last_3_months"));
        stringIdConstantMap.put(R.string.stiff_neck, FieldNameConstantMap.getFieldNameConstantMap().get("stiff_neck"));
        stringIdConstantMap.put(R.string.microscopic_rdt_positive, FieldNameConstantMap.getFieldNameConstantMap().get("microscopic_rdt_positive"));
        stringIdConstantMap.put(R.string.fever_other_region, FieldNameConstantMap.getFieldNameConstantMap().get("fever_other_region"));
        stringIdConstantMap.put(R.string.no_malaria_risk, FieldNameConstantMap.getFieldNameConstantMap().get("no_malaria_risk"));
        stringIdConstantMap.put(R.string.falciparum_negatve, FieldNameConstantMap.getFieldNameConstantMap().get("falciparum_negatve"));
        stringIdConstantMap.put(R.string.mesales_present, FieldNameConstantMap.getFieldNameConstantMap().get("mesales_present"));
        stringIdConstantMap.put(R.string.runny_nose, FieldNameConstantMap.getFieldNameConstantMap().get("runny_nose"));


        stringIdConstantMap.put(R.string.mouth_ulcers, FieldNameConstantMap.getFieldNameConstantMap().get("mouth_ulcers"));
        stringIdConstantMap.put(R.string.deep_and_extensive, FieldNameConstantMap.getFieldNameConstantMap().get("deep_and_extensive"));
        stringIdConstantMap.put(R.string.pus_draining_from_the_eye, FieldNameConstantMap.getFieldNameConstantMap().get("pus_draining_from_the_eye"));
        stringIdConstantMap.put(R.string.clouding_of_the_cornea, FieldNameConstantMap.getFieldNameConstantMap().get("clouding_of_the_cornea"));


        stringIdConstantMap.put(R.string.is_there_ear_pain, FieldNameConstantMap.getFieldNameConstantMap().get("mouth_ulcers"));
        stringIdConstantMap.put(R.string.pus_draining, FieldNameConstantMap.getFieldNameConstantMap().get("pus_draining"));
        stringIdConstantMap.put(R.string.tender_swelling_behind_the_ear, FieldNameConstantMap.getFieldNameConstantMap().get("tender_swelling_behind_the_ear"));
        stringIdConstantMap.put(R.string.discharge_more_than_14_days, FieldNameConstantMap.getFieldNameConstantMap().get("discharge_more_than_14_days"));
        stringIdConstantMap.put(R.string.discharge_less_than_14_days, FieldNameConstantMap.getFieldNameConstantMap().get("discharge_less_than_14_days"));
        stringIdConstantMap.put(R.string.is_there_ear_problem, FieldNameConstantMap.getFieldNameConstantMap().get("is_there_ear_problem"));


        stringIdConstantMap.put(R.string.oedema_of_both_feet, FieldNameConstantMap.getFieldNameConstantMap().get("oedema_of_both_feet"));
        stringIdConstantMap.put(R.string.wfh_less_than_3_z_scores, FieldNameConstantMap.getFieldNameConstantMap().get("wfh_less_than_3_z_scores"));
        stringIdConstantMap.put(R.string.wfh_between_2_3_z_scores, FieldNameConstantMap.getFieldNameConstantMap().get("wfh_between_2_3_z_scores"));
        stringIdConstantMap.put(R.string.wfh_between_2_or_more_z_scores, FieldNameConstantMap.getFieldNameConstantMap().get("wfh_between_2_or_more_z_scores"));
        stringIdConstantMap.put(R.string.MUAC_less_than_115_mm, FieldNameConstantMap.getFieldNameConstantMap().get("MUAC_less_than_115_mm"));
        stringIdConstantMap.put(R.string.MUAC_115_up_to_125_mm, FieldNameConstantMap.getFieldNameConstantMap().get("MUAC_115_up_to_125_mm"));
        stringIdConstantMap.put(R.string.MUAC_125_mm_or_more, FieldNameConstantMap.getFieldNameConstantMap().get("MUAC_125_mm_or_more"));
        stringIdConstantMap.put(R.string.Medical_complication_present, FieldNameConstantMap.getFieldNameConstantMap().get("Medical_complication_present"));
        stringIdConstantMap.put(R.string.weight_correct_as_height, FieldNameConstantMap.getFieldNameConstantMap().get("weight_correct_as_height"));


        stringIdConstantMap.put(R.string.severe_palmar_pallor , FieldNameConstantMap.getFieldNameConstantMap().get("severe_palmar_pallor"));
        stringIdConstantMap.put(R.string.some_pallor, FieldNameConstantMap.getFieldNameConstantMap().get("some_pallor"));
        stringIdConstantMap.put(R.string.no_whitness_in_palm, FieldNameConstantMap.getFieldNameConstantMap().get("no_whitness_in_palm"));

    /* start of less then 2 month */
        stringIdConstantMap.put(R.string.convulsions_two , FieldNameConstantMap.getFieldNameConstantMap().get("convulsions_two"));
        stringIdConstantMap.put(R.string.fastBreathing_two , FieldNameConstantMap.getFieldNameConstantMap().get("fastBreathing_two"));
        stringIdConstantMap.put(R.string.severe_chest_indrawing_two , FieldNameConstantMap.getFieldNameConstantMap().get("severe_chest_indrawing_two"));
        stringIdConstantMap.put(R.string.swollen_nose , FieldNameConstantMap.getFieldNameConstantMap().get("swollen_nose"));
        stringIdConstantMap.put(R.string.Not_feeding_well_two , FieldNameConstantMap.getFieldNameConstantMap().get("Not_feeding_well_two"));
        stringIdConstantMap.put(R.string.baby_palate_bulging_two , FieldNameConstantMap.getFieldNameConstantMap().get("baby_palate_bulging_two"));
        stringIdConstantMap.put(R.string.navel_reddish_skin_stretched_two , FieldNameConstantMap.getFieldNameConstantMap().get("navel_reddish_skin_stretched_two"));
        stringIdConstantMap.put(R.string.fever_two , FieldNameConstantMap.getFieldNameConstantMap().get("fever_two"));
        stringIdConstantMap.put(R.string.low_body_temperature_two , FieldNameConstantMap.getFieldNameConstantMap().get("low_body_temperature_two"));
        stringIdConstantMap.put(R.string.many_skin_vesciles_two , FieldNameConstantMap.getFieldNameConstantMap().get("many_skin_vesciles_two"));
        stringIdConstantMap.put(R.string.Umbilicus_red_pus_two , FieldNameConstantMap.getFieldNameConstantMap().get("Umbilicus_red_pus_two"));
        stringIdConstantMap.put(R.string.skin_pustules_two , FieldNameConstantMap.getFieldNameConstantMap().get("skin_pustules_two"));
        stringIdConstantMap.put(R.string.purulent_discharge_eye_two , FieldNameConstantMap.getFieldNameConstantMap().get("purulent_discharge_eye_two"));
        stringIdConstantMap.put(R.string.movement_only_two , FieldNameConstantMap.getFieldNameConstantMap().get("movement_only_two"));
        stringIdConstantMap.put(R.string.Yellow_palms_and_soles_two , FieldNameConstantMap.getFieldNameConstantMap().get("Yellow_palms_and_soles_two"));
        stringIdConstantMap.put(R.string.age_less_24_two , FieldNameConstantMap.getFieldNameConstantMap().get("age_less_24_two"));
        stringIdConstantMap.put(R.string.age_more_14_two , FieldNameConstantMap.getFieldNameConstantMap().get("age_more_14_two"));
        stringIdConstantMap.put(R.string.Palms_soles_not_yellow_two , FieldNameConstantMap.getFieldNameConstantMap().get("Palms_soles_not_yellow_two"));
        stringIdConstantMap.put(R.string.Jaundice_after_24_two , FieldNameConstantMap.getFieldNameConstantMap().get("Jaundice_after_24_two"));
        stringIdConstantMap.put(R.string.temperature_less_than_35_5_two , FieldNameConstantMap.getFieldNameConstantMap().get("temperature_less_than_35_5_two"));
        stringIdConstantMap.put(R.string.temperature_mild_two , FieldNameConstantMap.getFieldNameConstantMap().get("temperature_mild_two"));


        stringIdConstantMap.put(R.string.does_the_child_have_diarrhoea_two , FieldNameConstantMap.getFieldNameConstantMap().get("does_the_child_have_diarrhoea_two"));
        stringIdConstantMap.put(R.string.Slow_or_is_unconscious_two , FieldNameConstantMap.getFieldNameConstantMap().get("Slow_or_is_unconscious_two"));
        stringIdConstantMap.put(R.string.movement_no_two , FieldNameConstantMap.getFieldNameConstantMap().get("movement_no_two"));
        stringIdConstantMap.put(R.string.sunken_eyes_two , FieldNameConstantMap.getFieldNameConstantMap().get("sunken_eyes_two"));
        stringIdConstantMap.put(R.string.skin_pinch_very_slowly_two , FieldNameConstantMap.getFieldNameConstantMap().get("skin_pinch_very_slowly_two"));
        stringIdConstantMap.put(R.string.infant_age_less_than_month_two , FieldNameConstantMap.getFieldNameConstantMap().get("infant_age_less_than_month_two"));
        stringIdConstantMap.put(R.string.Restless_and_irritable_two , FieldNameConstantMap.getFieldNameConstantMap().get("Restless_and_irritable_two"));
        stringIdConstantMap.put(R.string.skin_pinch_slowly_two , FieldNameConstantMap.getFieldNameConstantMap().get("skin_pinch_slowly_two"));
        stringIdConstantMap.put(R.string.diarrhoea_more_14_two , FieldNameConstantMap.getFieldNameConstantMap().get("diarrhoea_more_14_two"));
        stringIdConstantMap.put(R.string.blood_stool_two , FieldNameConstantMap.getFieldNameConstantMap().get("blood_stool_two"));

        //stringIdConstantMap.put(R.string.feeding_problem_low_weight_two ",R.string.feeding_problem_low_weight_two"));
        stringIdConstantMap.put(R.string.Not_well_attached_breast_two , FieldNameConstantMap.getFieldNameConstantMap().get("Not_well_attached_breast_two"));
        stringIdConstantMap.put(R.string.Not_sucking_effectively_two , FieldNameConstantMap.getFieldNameConstantMap().get("Not_sucking_effectively_two"));
        stringIdConstantMap.put(R.string.lees_8_breastfeeds_2_two4 , FieldNameConstantMap.getFieldNameConstantMap().get("lees_8_breastfeeds_2_two4"));
        stringIdConstantMap.put(R.string.receives_foods_drinks_two , FieldNameConstantMap.getFieldNameConstantMap().get("receives_foods_drinks_two"));
        stringIdConstantMap.put(R.string.low_weight_age_two , FieldNameConstantMap.getFieldNameConstantMap().get("low_weight_age_two"));
        stringIdConstantMap.put(R.string.thrus_ulcer_patches_two , FieldNameConstantMap.getFieldNameConstantMap().get("thrus_ulcer_patches_two"));
        stringIdConstantMap.put(R.string.not_low_weight_two , FieldNameConstantMap.getFieldNameConstantMap().get("not_low_weight_two"));


        //classify
        stringIdConstantMap.put(R.string.classfy_very_sever_disease, FieldNameConstantMap.getFieldNameConstantMap().get("classfy_very_sever_disease"));
        stringIdConstantMap.put(R.string.classfy_SEVERE_PNEUMONIA_OR_VERY_SEVERE_DISEASE, FieldNameConstantMap.getFieldNameConstantMap().get("classfy_SEVERE_PNEUMONIA_OR_VERY_SEVERE_DISEASE"));
        stringIdConstantMap.put(R.string.classfy_PNEUMONIA, FieldNameConstantMap.getFieldNameConstantMap().get("classfy_PNEUMONIA"));
        stringIdConstantMap.put(R.string.classfy_cough_or_cold, FieldNameConstantMap.getFieldNameConstantMap().get("classfy_cough_or_cold"));

        stringIdConstantMap.put(R.string.classify_SEVERE_PERSISTENT_DIARRHOEA, FieldNameConstantMap.getFieldNameConstantMap().get("classify_SEVERE_PERSISTENT_DIARRHOEA"));
        stringIdConstantMap.put(R.string.classify_PERSISTENT_DIARRHOEA, FieldNameConstantMap.getFieldNameConstantMap().get("classify_PERSISTENT_DIARRHOEA"));
        stringIdConstantMap.put(R.string.classify_DYSENTERY, FieldNameConstantMap.getFieldNameConstantMap().get("classify_DYSENTERY"));
        stringIdConstantMap.put(R.string.classfy_sever_dehydration, FieldNameConstantMap.getFieldNameConstantMap().get("classfy_sever_dehydration"));
        stringIdConstantMap.put(R.string.classfy_some_dehydration, FieldNameConstantMap.getFieldNameConstantMap().get("classfy_some_dehydration"));
        stringIdConstantMap.put(R.string.classfy_no_dehydration, FieldNameConstantMap.getFieldNameConstantMap().get("classfy_no_dehydration"));

        stringIdConstantMap.put(R.string.classify_VERY_SEVERE_FEBRILE_DISEASE, FieldNameConstantMap.getFieldNameConstantMap().get("classify_VERY_SEVERE_FEBRILE_DISEASE"));
        stringIdConstantMap.put(R.string.classify_MALARIA_Falciparum, FieldNameConstantMap.getFieldNameConstantMap().get("classify_MALARIA_Falciparum"));
        stringIdConstantMap.put(R.string.classify_MALARIA_without_Falciparum, FieldNameConstantMap.getFieldNameConstantMap().get("classify_MALARIA_without_Falciparum"));
        stringIdConstantMap.put(R.string.classify_FEVER_no_malaria, FieldNameConstantMap.getFieldNameConstantMap().get("classify_FEVER_no_malaria"));
        stringIdConstantMap.put(R.string.classify_severe_complicated_measle, FieldNameConstantMap.getFieldNameConstantMap().get("classify_severe_complicated_measle"));
        stringIdConstantMap.put(R.string.classify_MEASLES_WITH_EYE_OR_MOUTH_COMPLICATIONS, FieldNameConstantMap.getFieldNameConstantMap().get("classify_MEASLES_WITH_EYE_OR_MOUTH_COMPLICATIONS"));
        stringIdConstantMap.put(R.string.classify_MEASLES, FieldNameConstantMap.getFieldNameConstantMap().get("classify_MEASLES"));
        stringIdConstantMap.put(R.string.classify_no_measelsVERY_SEVERE_FEBRILE_DISEASE, FieldNameConstantMap.getFieldNameConstantMap().get("classify_no_measelsVERY_SEVERE_FEBRILE_DISEASE"));
        stringIdConstantMap.put(R.string.classify_fever, FieldNameConstantMap.getFieldNameConstantMap().get("classify_fever"));

        stringIdConstantMap.put(R.string.classify_MASTOIDITIS, FieldNameConstantMap.getFieldNameConstantMap().get("classify_MASTOIDITIS"));
        stringIdConstantMap.put(R.string.classify_CHRONIC_EAR_INFECTION, FieldNameConstantMap.getFieldNameConstantMap().get("classify_CHRONIC_EAR_INFECTION"));
        stringIdConstantMap.put(R.string.classify_ACUTE_EAR_INFECTION, FieldNameConstantMap.getFieldNameConstantMap().get("classify_ACUTE_EAR_INFECTION"));
        stringIdConstantMap.put(R.string.classify_NO_EAR_INFECTION, FieldNameConstantMap.getFieldNameConstantMap().get("classify_NO_EAR_INFECTION"));


        stringIdConstantMap.put(R.string.classify_UNCOMPLICATED_SEVERE_ACUTE_MALNUTRITION, FieldNameConstantMap.getFieldNameConstantMap().get("classify_UNCOMPLICATED_SEVERE_ACUTE_MALNUTRITION"));
        stringIdConstantMap.put(R.string.classify_MODERATE_ACUTE_MALNUTRITION, FieldNameConstantMap.getFieldNameConstantMap().get("classify_MODERATE_ACUTE_MALNUTRITION"));
        stringIdConstantMap.put(R.string.classify_NO_ACUTE_MALNUTRITION, FieldNameConstantMap.getFieldNameConstantMap().get("classify_NO_ACUTE_MALNUTRITION"));

        stringIdConstantMap.put(R.string.classify_SEVERE_ANAEMIA, FieldNameConstantMap.getFieldNameConstantMap().get("classify_SEVERE_ANAEMIA"));
        stringIdConstantMap.put(R.string.classify_some_ANAEMIA, FieldNameConstantMap.getFieldNameConstantMap().get("classify_some_ANAEMIA"));
        stringIdConstantMap.put(R.string.classify_no_ANAEMIA, FieldNameConstantMap.getFieldNameConstantMap().get("classify_no_ANAEMIA"));
        return stringIdConstantMap;
    }

}
