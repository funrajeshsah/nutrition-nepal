package com.nepget.rhino;

/**
 * Created by rajesh on 14/7/16.
 */
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.Log;

import java.util.Locale;

/**
 * This class is used to change your application locale and persist this change for the next time
 * that your app is going to be used.
 * <p/>
 * You can also change the locale of your application on the fly by using the setLocale method.
 * <p/>
 * Created by gunhansancar on 07/10/15.
 */
public class LocaleHelper {

    private static final String SELECTED_LANGUAGE_PREFERENCE = "Locale.Helper.Selected.Language";
    private static final String SELECTED_LANGUAGE_KEY = "selected_language";

    static String TAG = "LocaleHelper";
    static SharedPreferences settings;
    static SharedPreferences.Editor editor;

    public static void  onCreate(Context context) {
        Log.d(TAG, "in onCreate ");
        /*settings = context.getSharedPreferences(SELECTED_LANGUAGE_PREFERENCE, Context.MODE_PRIVATE);
        if ((null == settings ) || (null == settings.getString(SELECTED_LANGUAGE_KEY,null))){
            Log.d(TAG, "in onCreate : setting null ");
            settings = context.getSharedPreferences(SELECTED_LANGUAGE_PREFERENCE, Context.MODE_PRIVATE);
            Log.d(TAG, "in onCreate : before setLocale ");
            //setLocale(context, lang);
            Log.d(TAG, "in onCreate : after setLocale ");
        }
        updateResources(context, settings.getString(SELECTED_LANGUAGE_KEY,null));*/
    }
    public static void  onCreate(Context context, String lang) {
        /*Log.d(TAG, "in onCreate : "+lang);
        settings = context.getSharedPreferences(SELECTED_LANGUAGE_PREFERENCE, Context.MODE_PRIVATE);
        if ((null == settings ) || (null == settings.getString(SELECTED_LANGUAGE_KEY,null))){
            Log.d(TAG, "in onCreate : setting null ");
            settings = context.getSharedPreferences(SELECTED_LANGUAGE_PREFERENCE, Context.MODE_PRIVATE);
            Log.d(TAG, "in onCreate : before setLocale ");
            setLocale(context, lang);
            Log.d(TAG, "in onCreate : after setLocale ");
        }
        updateResources(context, settings.getString(SELECTED_LANGUAGE_KEY,null));*/
    }

    public static boolean isFirstTime(Context context){
        settings = context.getSharedPreferences(SELECTED_LANGUAGE_PREFERENCE, Context.MODE_PRIVATE);
        if (null == settings){
            return true;
        }
        return false;
    }

    public static void setLocale(Context context, String language){
        Log.d(TAG, "in setLocale : " +language);
        editor = settings.edit();
        editor.clear();
        editor.putString(SELECTED_LANGUAGE_KEY, language);
        editor.commit();
        Log.d(TAG, "in after commit : " +language);
        updateResources( context, language);
        Log.d(TAG, "in after updateResources : " +language);
    }

    private static void updateResources(Context context, String language) {
        /*Log.d(TAG, "in updateResources : "+language);
        Locale locale = new Locale(language);
        Locale.setDefault(locale);

        Resources resources = context.getResources();

        Configuration configuration = resources.getConfiguration();
        configuration.locale = locale;

        resources.updateConfiguration(configuration, resources.getDisplayMetrics());*/
    }

}