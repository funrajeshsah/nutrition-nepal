package com.nepget.rhino;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;

public class NavResult extends BaseActivity {

    private WebView webview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        super.onCreateDrawer();
        LocaleHelper.onCreate(this);

        FrameLayout frame = (FrameLayout) findViewById(R.id.containt_framme);
        View childView =  getLayoutInflater().inflate(R.layout.content_nav_result,null);
        frame.addView(childView);

        Intent intent = getIntent();
        String item_name  = (String)intent.getStringExtra("item_name");
        String source = (String)intent.getStringExtra("source");
        TouchImageView image = (TouchImageView) findViewById(R.id.nav_result_image);
        if(item_name.equalsIgnoreCase("nav_principle_of_imnci")){
           String html_file_path = "file:///android_asset/www/info/principle.html";
            displayWebView(html_file_path);
        }
        else if(item_name.equalsIgnoreCase("about_us")){
            String html_file_path = "file:///android_asset/www/info/about_us.html";
            displayWebView(html_file_path);
        }
        else if(item_name.equalsIgnoreCase("contact_us")){
            String html_file_path = "file:///android_asset/www/info/contact_us.html";
            displayWebView(html_file_path);
        }
        else if(item_name.equalsIgnoreCase("terms_and_condition")){
            String html_file_path = "file:///android_asset/www/info/terms_and_condition.html";
            displayWebView(html_file_path);
        }
        else {
            String html_content = "No content found";
            if(source.equalsIgnoreCase("book_chart")){
                html_content = ServerData.getInstance().getBooklet().getSubheadingMap().get(item_name).getHtml();
            }else {
                html_content = ServerData.getInstance().getDoase().getSubheadingMap().get(item_name).getHtml();
            }
            displayWebView(html_content,true);
        }

    }

    private  void displayScrollView(){
        TouchImageView scrollView = (TouchImageView) findViewById(R.id.nav_result_image);
        scrollView.setVisibility(View.VISIBLE);
    }

    //TODO merge two function , added this function for backward compability
    private  void displayWebView(String html_content, boolean isHtml){
        webview = (WebView) findViewById(R.id.webView);
        WebSettings webSettings = webview.getSettings();
        webview.setWebViewClient(new MyAppWebViewClient());
        webview.setVisibility(View.VISIBLE);
        webSettings.setJavaScriptEnabled(true);
        webSettings.setBuiltInZoomControls(true);
       // webview.loadUrl(html_file_path);

        //webview.loadData("<html><body><h1>Hello World!</h1></body></html>", "text/html", "UTF-8");
        webview.loadData(html_content, "text/html", "UTF-8");

    }

    private  void displayWebView(String html_file_path){
        webview = (WebView) findViewById(R.id.webView);
        WebSettings webSettings = webview.getSettings();
        webview.setWebViewClient(new MyAppWebViewClient());
        webview.setVisibility(View.VISIBLE);
        webSettings.setJavaScriptEnabled(true);
        webSettings.setBuiltInZoomControls(true);
        webview.loadUrl(html_file_path);

    }

    /*@Override
    public void onBackPressed()
    {
        Intent intent = new Intent(this, Home.class);
        finish(); // to simulate "restart" of the activity.
        startActivity(intent);
    }*/

    /*@Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(webview == null){
            Intent intent = new Intent(this, Home.class);
            finish(); // to simulate "restart" of the activity.
            startActivity(intent);
        }
        else if ((keyCode == KeyEvent.KEYCODE_BACK) && webview.canGoBack()) {
            //if Back key pressed and webview can navigate to previous page
            webview.goBack();
            // go back to previous page
            return true;
        }
        else {
                Intent intent = new Intent(this, Home.class);
                finish(); // to simulate "restart" of the activity.
                startActivity(intent);
        }
        return super.onKeyDown(keyCode, event);
    }*/

     class MyAppWebViewClient extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            //if(Uri.parse(url).getHost().endsWith("html5rocks.com")) {
                return false;
            //}
        }
    }

}
