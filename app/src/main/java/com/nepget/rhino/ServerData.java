package com.nepget.rhino;

import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.nepget.rhino.utility.Booklet;
import com.nepget.rhino.utility.Treatment;
import com.nepget.rhino.utility.User;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by rajesh on 26/1/17.
 */
public class ServerData {

    private String TAG = "ServerData";

    private static ServerData ourInstance = new ServerData();

    private Booklet booklet = new Booklet();
    private Treatment doase = new Treatment();

    public static ServerData getInstance() {
        return ourInstance;
    }

    private ServerData() {
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference bookRef = database.getReference("Booklet");

        //TODO change treatment to doase
        //Treatment is alis of doase
        DatabaseReference doaseRef = database.getReference("Treatment");
        bookRef.keepSynced(true);
        doaseRef.keepSynced(true);
        bookRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                booklet = dataSnapshot.getValue(Booklet.class);
                Log.d(TAG, "Value is: " + booklet);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });

        doaseRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                 doase = dataSnapshot.getValue(Treatment.class);
                Log.d(TAG, "Value is: " + doase);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });
    }

    public Booklet getBooklet() {
        return booklet;
    }

    public void setBooklet(Booklet booklet) {
        this.booklet = booklet;
    }

    public Treatment getDoase() {
        return doase;
    }

    public void setDoase(Treatment doase) {
        this.doase = doase;
    }



    public void writeNewUser(String userId, String name, String email) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        User user = new User(name, email,dateFormat.format(date));
        DatabaseReference mDatabase;
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.child("users").child(userId).setValue(user);
    }
}
